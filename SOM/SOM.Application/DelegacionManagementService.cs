﻿using SOM.Business;
using SOM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SOM.Application
{
    public class DelegacionManagementService
    {
        public List<Delegacion> GetDelegaciones()
        {
            return new DelegacionBusiness().GetDelegaciones();
        }
    }
}