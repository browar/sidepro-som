﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SOM.Business;
namespace SOM.Application
{
    public class HelperManagementService
    {
        public bool enviarMail(int id_usuario, string From, string To, string ReplyTo, string Subject, string Message)
        {
            HelperBusiness miHelper = new HelperBusiness();
            return miHelper.enviarMail(id_usuario, From, To, ReplyTo, Subject, Message);
        }
        public string saveFile (int id_modulo, int id_curso, int id_leccion, int tipo_archivo, string nombre){
            HelperBusiness miHelper = new HelperBusiness();
            return miHelper.saveFile(id_modulo, id_curso, id_leccion, tipo_archivo, nombre);
        }
    }
}