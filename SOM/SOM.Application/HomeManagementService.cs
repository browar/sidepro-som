﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SOM.Business;
using SOM.Entities;
namespace SOM.Application
{
  
    public class HomeManagementService
    {
        HomeBusiness homeBusiness = new HomeBusiness();
        public Usuario getUser(string user, string pass)
        {
            return homeBusiness.getUser(user, pass);
        }
        public List<Certification> getCertificaciones(int id_usuario) {
            return homeBusiness.getCertificaciones(id_usuario);
        }
        public List<Destacado> getDestacados(int id_usuario)
        {
            return homeBusiness.getDestacados(id_usuario);
        }
        public string CambiarPassword(int id_usuario, string pass_actual, string pass_nueva)
        {
            return homeBusiness.CambiarPassword(id_usuario, pass_actual, pass_nueva);
        }
        public Usuario recuperarPassword(string Email)
        {
            return homeBusiness.recuperarPassword(Email);
        }

    }
}