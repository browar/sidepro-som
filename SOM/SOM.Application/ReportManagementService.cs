﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SOM.Entities;
using SOM.Business;
namespace SOM.Application
{
    public class ReportManagementService
    {
        ReportBusiness reportBusiness = new ReportBusiness();
        public CourseProgress getProgresoReportes(int id_usuario)
        {
            return reportBusiness.getProgresoReportes(id_usuario);
        }
        public CourseProgress getProgresoAgenciaReportes(int id_usuario, int id_agencia) {
            return reportBusiness.getProgresoReportesAgencia(id_usuario, id_agencia);
        }   
        public List<CourseProgress> getProgresoMensual(int id_usuario)
        {
            return reportBusiness.getProgresoMensual(id_usuario);
        }
        public List<CourseProgress> getCertificacionMensual(int id_usuario)
        {
            return reportBusiness.getCertificacionMensual(id_usuario);
        }

        
        public List<CourseProgress> getLogeoUsuarios()
        {
            return reportBusiness.getLogeoUsuarios();
        }
        
        public List<CourseProgress> getProgresoMensualAgencia(int id_agencia)
        {
            return reportBusiness.getProgresoMensualAgencia(id_agencia);
        }
        public List<CourseProgress> getProgresoMensualUsuariosAgencia(int id_agencia)
        {
            return reportBusiness.getProgresoMensualUsuariosAgencia(id_agencia);
        }
        public List<AgenteProgress> getProgresoAgente(int id_usuario)
        {
            return reportBusiness.getProgresoAgente(id_usuario);
        }
        public AgenteProgress getProgresoAgencia(int id_usuario)
        {
            return reportBusiness.getProgresoAgencia(id_usuario);
        }
        public AgenteProgress getProgresoCec(int id_usuario)
        {
            return reportBusiness.getProgresoCec(id_usuario);
        }
        public List<AgenteProgress> getProgresoAgente(int desde, int hasta)
        {
            return reportBusiness.getProgresoAgente(desde, hasta);
        }
        public List<UserProgress> getProgresoUsuarios()
        {
            return reportBusiness.getProgresoUsuarios();
        }
        public List<UserProgress> getProgresoUsuarios(int id_agencia)
        {
            return reportBusiness.getProgresoUsuarios(id_agencia);
        }
        public UserProgress getProgresoUsuario(int id_agencia)
        {
            return reportBusiness.getProgresoUsuario(id_agencia);
        }        
        public List<UserProgress> getProgresoUsuarios(string usuario)
        {
            return reportBusiness.getProgresoUsuarios(usuario);
        }
        public List<UserProgress> getProgresoUsuarios(string usuario, int id_agencia)
        {
            return reportBusiness.getProgresoUsuarios(usuario, id_agencia);
        }
        public List<UserProgress> getCertificacionesDeUsuariosDeAgenciaDelMes(int id_agencia, int mes, int ano)
        {
            return reportBusiness.getCertificacionesDeUsuariosDeAgenciaDelMes(id_agencia, mes, ano);
        }
        
        public DatosReporte getDatosReporte( )
        {
            return reportBusiness.getDatosReporte();
        }
        
    }
}