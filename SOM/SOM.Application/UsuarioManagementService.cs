﻿using SOM.Business;
using SOM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SOM.Application
{
    public class UsuarioManagementService
    {
        public List<Usuario> getUsuarios()
        {
            return new UsuarioBusiness().getUsuarios();
        }
        public string cambiarEstado(int id_usuario, int cod_estado)
        {
            return new UsuarioBusiness().cambiarEstado(id_usuario, cod_estado);
        }
        public string agregarUsuario(string txt_user, string nombre, string apellido,string password,  string email, string dni, int cod_rol, int id_agencia)
        {
            return new UsuarioBusiness().agregarUsuario(txt_user, nombre, apellido, password, email, dni, cod_rol, id_agencia);
        }
        public string modificarUsuario(int id_usuario, string nombre, string apellido, string password, string email, string dni, int sn_activo, int cod_rol, int id_agencia)
        {
            return new UsuarioBusiness().modificarUsuario(id_usuario, nombre, apellido, password, email, dni, sn_activo, cod_rol, id_agencia);
        }
        public Usuario registrarUsuario(string txt_user, string nombre, string apellido, string password, string email, string dni, int delegacion){
            return new UsuarioBusiness().registrarUsuario(txt_user, nombre, apellido, password, email, dni, delegacion);
        }
    }
}