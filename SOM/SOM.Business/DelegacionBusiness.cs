﻿using SOM.Data;
using SOM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SOM.Business
{
    public class DelegacionBusiness
    {
        public List<Delegacion> GetDelegaciones()
        {
            return new DelegacionData().GetDelegaciones();
        }
    }
}