﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SOM.Data;
using SOM.Data.Helpers;
namespace SOM.Business
{
    public class HelperBusiness
    {
        public bool enviarMail(int id_usuario, string From, string To, string ReplyTo, string Subject, string Message)
        { 
            HelpersData miHelper = new HelpersData();
            return miHelper.enviarMail(id_usuario, From, To, ReplyTo, Subject, Message);
        }
        public string saveFile(int id_modulo, int id_curso, int id_leccion, int tipo_archivo, string nombre)
        {
            HelpersData miHelper = new HelpersData();
            return miHelper.saveFile(id_modulo, id_curso, id_leccion, tipo_archivo, nombre);
        }
    }
}