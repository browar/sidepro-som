﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SOM.Data;
using SOM.Entities;
namespace SOM.Business
{
    public class HomeBusiness
    {
        HomeData homeData = new HomeData();
        public Usuario getUser(string user, string pass)
        {
            return homeData.getUser(user, pass);
        }
        public List<Certification> getCertificaciones(int id_usuario)
        {
            return homeData.getCertificaciones(id_usuario);
        }
        public List<Destacado> getDestacados(int id_usuario)
        {
            return homeData.getDestacados(id_usuario);
        }
        public string CambiarPassword(int id_usuario, string pass_actual, string pass_nueva)
        {
            return homeData.CambiarPassword(id_usuario, pass_actual, pass_nueva);
        }
        public Usuario recuperarPassword(string Email)
        {
            return homeData.recuperarPassword(Email);
        }

    }
}