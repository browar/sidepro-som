﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SOM.Entities;
using System.Data.OleDb;
namespace SOM.Data
{
    public class DelegacionData
    {
        public List<Delegacion> GetDelegaciones()
        {

            List<Delegacion> miLista = new List<Delegacion>();
            Delegacion miDelegacion;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_delegaciones");
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miDelegacion = new Delegacion();
                    miDelegacion.Codigo = Convert.ToInt32(miDataReader["Codigo"]);
                    miDelegacion.Descripcion = miDataReader["Descripcion"].ToString();
                    miLista.Add(miDelegacion);
                }
            }

            miConn.Close();
            return miLista;
        }

    }
}