﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Net.Mail;
using System.Net;
namespace SOM.Data.Helpers
{
    public class HelpersData
    {
        public static OleDbDataReader getDataReader(OleDbConnection miConn, string query)
        {
            OleDbCommand miCommand = new OleDbCommand();
            OleDbDataReader miDataReader;

            miCommand.Connection = miConn;
            miCommand.CommandType = CommandType.StoredProcedure;
            miCommand.CommandText = query;
            miDataReader = miCommand.ExecuteReader();
            return miDataReader;
        }

        public static OleDbDataReader getDataReader(OleDbConnection miConn, string query, OleDbParameter[] misParametros) {
            OleDbCommand miCommand = new OleDbCommand();
            OleDbDataReader miDataReader;

            miCommand.Connection = miConn;
            miCommand.CommandType = CommandType.StoredProcedure;
            miCommand.CommandText = query;
            
            foreach (OleDbParameter miParam in misParametros)
                miCommand.Parameters.Add(miParam);
            
            miDataReader = miCommand.ExecuteReader();
            return miDataReader;
        }
        public static OleDbConnection getConnection() {
            OleDbConnection miConexion = new OleDbConnection();
            miConexion.ConnectionString = ConfigurationManager.ConnectionStrings["Sidepro-SOM"].ConnectionString;
            miConexion.Open();
            return miConexion;
         
        }
        public bool enviarMail(int id_usuario, string From, string To, string ReplyTo, string Subject, string Message)
        {

            try {
                string pepe = SendUserMail(From, To, Message, From, Subject);
                OleDbConnection miConn = getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[2];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_usuario";
                miParametro.Value = id_usuario;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_consulta";
                miParametro.Value = Message;
                misParametros[1] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_enviar_consulta", misParametros);
                miDataReader.Close();
                miConn.Close();

                return true;
            }catch{
                return false;
            }
            
        }
        public string saveFile(int id_modulo, int id_curso, int id_leccion, int tipo_archivo, string nombre)
        {
            string resultado = "";
            try
            {
                OleDbConnection miConn = getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[5];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@tipo_archivo";
                miParametro.Value = tipo_archivo;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@original_file_name";
                miParametro.Value = nombre;
                misParametros[4] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_subir_archivo", misParametros);

                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    resultado = miDataReader["fileName"].ToString();
                }
                
                miDataReader.Close();
                miConn.Close();

                return resultado;
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }        
        public string SendUserMail(string fromad, string toad, string body, string header, string subjectcontent)
        {
            string result = "";
            //MailMessage usermail = Mailbodplain(fromad, toad, body, header, subjectcontent);
            //SmtpClient client = new SmtpClient();
            //Add the Creddentials- use your own email id and password
            //client.Credentials = new System.Net.NetworkCredential("noreplysidepro@gmail.com", "Agus221#"); ;

            //client.Host = "smtp.gmail.com";// "s184-168-147-58.secureserver.net";// "smtp.gmail.com";
            //client.Port = 25;
            //client.EnableSsl = true;
            try
            {
                MailAddress to = new MailAddress(toad); // new MailAddress(Console.ReadLine());
                MailAddress from = new MailAddress("no-reply@sidepro.com.ar");//new MailAddress("noreplysidepro@gmail.com");
                MailMessage mail = new MailMessage(from, to);
                mail.Subject = "Consulta";
                mail.Body = body;//"Testeo de Mails puerto 25 9.36 intento unico";// Console.ReadLine();
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "s184-168-147-58.secureserver.net";
                smtp.Port = 25;
                smtp.Credentials = new NetworkCredential("no-reply@sidepro.com.ar", "Agus221#");
                smtp.EnableSsl = false;
                smtp.Send(mail);

                result = "todoOk";
            }
            catch (Exception ex)
            {
                result = ex.Message;
            } // end try


            return result;

        }
        public MailMessage Mailbodplain(string fromad, string toad, string body, string header, string subjectcontent)
        {
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            try
            {
                string from = fromad;
                string to = toad;
                mail.To.Add(to);
                mail.From = new MailAddress(from, header, System.Text.Encoding.UTF8);
                mail.Subject = subjectcontent;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Body = body;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;
            }
            catch 
            {
                throw;
            }
            return mail;
        }

        public static void sendEmailAltas(string txt_mail, string txt_user, string txt_nombre, string txt_apellido, string txt_password, string nro_doc)
        {
            MailAddress to = new MailAddress(txt_mail); // new MailAddress(Console.ReadLine());
            MailAddress from = new MailAddress("no-reply@sidepro.com.ar");//new MailAddress("noreplysidepro@gmail.com");
            MailMessage mail = new MailMessage(from, to);
            mail.IsBodyHtml = true;

            List<MailAddress> misCopias =  new List<MailAddress>();
            misCopias.Add(new MailAddress("no-reply@sidepro.com.ar"));
            foreach (MailAddress miAdress in misCopias)
            {
                mail.Bcc.Add(miAdress);
            }


            string body = "";

            mail.Subject = "Alta usuario - Sidepro";
            #region -body-
            body = body + @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>";
            body = body + @" 	<html xmlns='http://www.w3.org/1999/xhtml'>";
            body = body + @"	    <head>";
            body = body + @"	        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
            body = body + @"	        <title>Sidepro - Alta de usuario</title>";
            body = body + @"	        <style type='text/css'>";
            body = body + @"	            /* /\/\/\/\/\/\/\/\/ CLIENT-SPECIFIC STYLES /\/\/\/\/\/\/\/\/ */";
            body = body + @"	            #outlook a{padding:0;} /* Force Outlook to provide a 'view in browser' message */";
            body = body + @"	            .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */";
            body = body + @"	            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */";
            body = body + @"	            body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */";
            body = body + @"	            table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */";
            body = body + @"	            img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */";

            body = body + @"	            /* /\/\/\/\/\/\/\/\/ RESET STYLES /\/\/\/\/\/\/\/\/ */";
            body = body + @"	            body{margin:0; padding:0;}";
            body = body + @"	            img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}";
            body = body + @"	            table{border-collapse:collapse !important;}";
            body = body + @"	            body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}";

            body = body + @"	            /* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */";

            body = body + @"	            #bodyCell{padding:20px;}";
            body = body + @"	            #templateContainer{width:600px;}";

            body = body + @"	            /* ========== Page Styles ========== */";
            body = body + @"	            ";
            body = body + @"	            html {";
            body = body + @"	                min-height: 100%;";
            body = body + @"	            }";

            body = body + @"	            body {";
            body = body + @"	                background-color: #129DC0;";
            body = body + @"	                background-image: url(bg.jpg);";
            body = body + @"	                background-size: cover;";
            body = body + @"	                background-repeat: no-repeat;";
            body = body + @"	            }";

            body = body + @"	            h1{";
            body = body + @"	                color:#202020 !important;";
            body = body + @"	                display:block;";
            body = body + @"	                font-family:Helvetica;";
            body = body + @"	                font-size:26px;";
            body = body + @"	                font-style:normal;";
            body = body + @"	                font-weight:bold;";
            body = body + @"	                line-height:100%;";
            body = body + @"	                letter-spacing:normal;";
            body = body + @"	                margin-top:0;";
            body = body + @"	                margin-right:0;";
            body = body + @"	                margin-bottom:10px;";
            body = body + @"	                margin-left:0;";
            body = body + @"	                text-align:left;";
            body = body + @"	            }";

            body = body + @"	            h2{";
            body = body + @"	                color:#404040 !important;";
            body = body + @"	                display:block;";
            body = body + @"	                font-family:Helvetica;";
            body = body + @"	                font-size:20px;";
            body = body + @"	                font-style:normal;";
            body = body + @"	                font-weight:bold;";
            body = body + @"	                line-height:100%;";
            body = body + @"	                letter-spacing:normal;";
            body = body + @"	                margin-top:0;";
            body = body + @"	                margin-right:0;";
            body = body + @"	                margin-bottom:10px;";
            body = body + @"	                margin-left:0;";
            body = body + @"	                text-align:left;";
            body = body + @"	            }";

            body = body + @"	            h3{";
            body = body + @"	                color:#606060 !important;";
            body = body + @"	                display:block;";
            body = body + @"	                font-family:Helvetica;";
            body = body + @"	                font-size:16px;";
            body = body + @"	                font-style:italic;";
            body = body + @"	                font-weight:normal;";
            body = body + @"	                line-height:100%;";
            body = body + @"	                letter-spacing:normal;";
            body = body + @"	                margin-top:0;";
            body = body + @"	                margin-right:0;";
            body = body + @"	                margin-bottom:10px;";
            body = body + @"	                margin-left:0;";
            body = body + @"	                text-align:left;";
            body = body + @"	            }";

            body = body + @"	            h4{";
            body = body + @"	                color:#808080 !important;";
            body = body + @"	                display:block;";
            body = body + @"	                font-family:Helvetica;";
            body = body + @"	                font-size:14px;";
            body = body + @"	                font-style:italic;";
            body = body + @"	                font-weight:normal;";
            body = body + @"	                line-height:100%;";
            body = body + @"	                letter-spacing:normal;";
            body = body + @"	                margin-top:0;";
            body = body + @"	                margin-right:0;";
            body = body + @"	                margin-bottom:10px;";
            body = body + @"	                margin-left:0;";
            body = body + @"	                text-align:left;";
            body = body + @"	            }";

            body = body + @"	            /* ========== Header Styles ========== */";

            body = body + @"	            #templatePreheader{";
            body = body + @"	            }";

            body = body + @"	            .preheaderContent{";
            body = body + @"	                color:#ffffff;";
            body = body + @"	                font-family:Helvetica;";
            body = body + @"	                font-size:12px;";
            body = body + @"	                line-height:125%;";
            body = body + @"	                text-align:left;";
            body = body + @"	            }";

            body = body + @"	            .preheaderContent a:link, .preheaderContent a:visited, /* Yahoo! Mail Override */ .preheaderContent a .yshortcuts /* Yahoo! Mail Override */{";
            body = body + @"	                color:#ffffff;";
            body = body + @"	                font-weight:normal;";
            body = body + @"	                text-decoration:underline;";
            body = body + @"	            }";

            body = body + @"	            .headerContent{";
            body = body + @"	                color:#505050;";
            body = body + @"	                font-family:Helvetica;";
            body = body + @"	                font-size:20px;";
            body = body + @"	                font-weight:bold;";
            body = body + @"	                line-height:100%;";
            body = body + @"	                padding-top:0;";
            body = body + @"	                padding-right:0;";
            body = body + @"	                padding-bottom:0;";
            body = body + @"	                padding-left:0;";
            body = body + @"	                text-align:left;";
            body = body + @"	                vertical-align:middle;";
            body = body + @"	            }";

            body = body + @"	            .headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{";
            body = body + @"	                color:#EB4102;";
            body = body + @"	                font-weight:normal;";
            body = body + @"	                text-decoration:underline;";
            body = body + @"	            }";

            body = body + @"	            #headerImage{";
            body = body + @"	                height:auto;";
            body = body + @"	                max-width:600px;";
            body = body + @"	            }";

            body = body + @"	            /* ========== Body Styles ========== */";

            body = body + @"	            #templateBody{";
            body = body + @"	                background-color:#FFFFFF;";
            body = body + @"	            }";

            body = body + @"	            .bodyContent{";
            body = body + @"	                color:#505050;";
            body = body + @"	                font-family:Helvetica;";
            body = body + @"	                font-size:14px;";
            body = body + @"	                line-height:150%;";
            body = body + @"	                padding-top:30px;";
            body = body + @"	                padding-right:30px;";
            body = body + @"	                padding-bottom:30px;";
            body = body + @"	                padding-left:30px;";
            body = body + @"	                text-align:left;";
            body = body + @"	            }";

            body = body + @"	            .bodyContent a:link, .bodyContent a:visited, /* Yahoo! Mail Override */ .bodyContent a .yshortcuts /* Yahoo! Mail Override */{";
            body = body + @"	                color:#EB4102;";
            body = body + @"	                font-weight:normal;";
            body = body + @"	                text-decoration:underline;";
            body = body + @"	            }";

            body = body + @"	            .bodyContent img{";
            body = body + @"	                display:inline;";
            body = body + @"	                height:auto;";
            body = body + @"	                max-width:560px;";
            body = body + @"	            }";

            body = body + @"	            /* ========== Template main content ========== */";

            body = body + @"	            #templateMainContent{";
            body = body + @"	                /* box-shadow: 0px 2px 2px #DEDEDE, 0px 3px 12px #D4D4D4; */";
            body = body + @"	            }";

            body = body + @"	            /* ========== Footer Styles ========== */";


            body = body + @"	            #templateFooter{";
            body = body + @"	                background-color:transparent;";
            body = body + @"	            }";

            body = body + @"	            .footerContent{";
            body = body + @"	                color:#ffffff;";
            body = body + @"	                font-family:Helvetica;";
            body = body + @"	                font-size:14px;";
            body = body + @"	                line-height:150%;";
            body = body + @"	                padding-top:20px;";
            body = body + @"	                padding-right:20px;";
            body = body + @"	                padding-bottom:20px;";
            body = body + @"	                padding-left:20px;";
            body = body + @"	                text-align:left;";
            body = body + @"	            }";

            body = body + @"	            .footerContent a:link, .footerContent a:visited, /* Yahoo! Mail Override */ .footerContent a .yshortcuts, .footerContent a span /* Yahoo! Mail Override */{";
            body = body + @"	                color:#ffffff;";
            body = body + @"	                font-weight:normal;";
            body = body + @"	                text-decoration:underline;";
            body = body + @"	            }";

            body = body + @"	            hr {";
            body = body + @"	                border: 1px solid #E2E2E2;";
            body = body + @"	                margin: 25px 0;";
            body = body + @"	            }";

            body = body + @"	            .userData {";
            body = body + @"	                width: 50%;";
            body = body + @"	                padding-bottom: 18px;";
            body = body + @"	            }";

            body = body + @"	            .userData p {";
            body = body + @"	                margin: 0;";
            body = body + @"	                line-height: 24px;";
            body = body + @"	            }";

            body = body + @"	            /* /\/\/\/\/\/\/\/\/ MOBILE STYLES /\/\/\/\/\/\/\/\/ */";

            body = body + @"	            @media only screen and (max-width: 480px){";
            body = body + @"	                /* /\/\/\/\/\/\/ CLIENT-SPECIFIC MOBILE STYLES /\/\/\/\/\/\/ */";
            body = body + @"	                body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */";
            body = body + @"	                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */";

            body = body + @"	                /* /\/\/\/\/\/\/ MOBILE RESET STYLES /\/\/\/\/\/\/ */";
            body = body + @"	                #bodyCell{padding:10px !important;}";

            body = body + @"	                /* /\/\/\/\/\/\/ MOBILE TEMPLATE STYLES /\/\/\/\/\/\/ */";

            body = body + @"	                /* ======== Page Styles ======== */";

            body = body + @"	                #templateContainer{";
            body = body + @"	                    max-width:600px !important;";
            body = body + @"	                    width:100% !important;";
            body = body + @"	                }";

            body = body + @"	                /* ======== Header Styles ======== */";

            body = body + @"	                #templatePreheader{display:none !important;} /* Hide the template preheader to save space */";

            body = body + @"	                #headerImage{";
            body = body + @"	                    height:auto !important;";
            body = body + @"	                    max-width:600px !important;";
            body = body + @"	                    width:100% !important;";
            body = body + @"	                }";

            body = body + @"	                .headerContent{";
            body = body + @"	                    font-size:20px !important;";
            body = body + @"	                    line-height:125% !important;";
            body = body + @"	                }";

            body = body + @"	                /* ======== Body Styles ======== */";

            body = body + @"	                #bodyImage{";
            body = body + @"	                    height:auto !important;";
            body = body + @"	                    max-width:560px !important;";
            body = body + @"	                    width:100% !important;";
            body = body + @"	                }";

            body = body + @"	                /* ======== Footer Styles ======== */";

            body = body + @"	                .footerContent{";
            body = body + @"	                    font-size:14px !important;";
            body = body + @"	                    line-height:115% !important;";
            body = body + @"	                }";
            body = body + @"	            }";
            body = body + @"	        </style>";
            body = body + @"	    </head>";
            body = body + @"	    <body leftmargin='0' marginwidth='0' topmargin='0' marginheight='0' offset='0'>";
            body = body + @"	        <center>";
            body = body + @"	            <table align='center' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' id='bodyTable'>";
            body = body + @"	                <tr>";
            body = body + @"	                    <td align='center' valign='top' id='bodyCell'>";
            body = body + @"	                        <!-- BEGIN TEMPLATE // -->";
            body = body + @"	                        <table border='0' cellpadding='0' cellspacing='0' id='templateContainer'>";
            body = body + @"	                            <tr>";
            body = body + @"	                                <td>";
            body = body + @"	                                    <table border='0' cellpadding='0' cellspacing='0' width='100%' id='templateMainContent'>";
            body = body + @"	                                        <tr>";
            body = body + @"	                                            <td align='center' valign='top'>";
            body = body + @"	                                                <!-- BEGIN HEADER // -->";

            body = body + @"	                                                <!-- // END HEADER -->";
            body = body + @"	                                            </td>";
            body = body + @"	                                        </tr>";
            body = body + @"	                                        <tr>";
            body = body + @"	                                            <td align='center' valign='top'>";
            body = body + @"	                                                <!-- BEGIN BODY // -->";
            body = body + @"	                                                <table border='0' cellpadding='0' cellspacing='0' width='100%' id='templateBody'>";
            body = body + @"	                                                    <tr>";
            body = body + @"	                                                        <td valign='top' class='bodyContent'>";
            body = body + @"	                                                            <h2>Alta de usuario - Portal training</h2>";
            body = body + @"	                                                            <p>A continuación podés encontrar los datos de tu cuenta.</p>";
            body = body + @"	                                                            <hr />";

            body = body + @"	                                                            <table border='0' cellpadding='0' cellspacing='0' width='100%' id='templateBody'>";
            body = body + @"	                                                                <tr>";
            body = body + @"	                                                                    <td valign='top' class='userData'>";
            body = body + @"	                                                                        <p><strong>Usuario</strong></p>";
            body = body + @"	                                                                        <p>" + txt_user + "</p>";
            body = body + @"	                                                                    </td>";
            body = body + @"	                                                                    <td valign='top' class='userData'>";
            body = body + @"	                                                                        <p><strong>Email</strong></p>";
            body = body + @"	                                                                        <p>" + txt_mail + "</p>";
            body = body + @"	                                                                    </td>";
            body = body + @"	                                                                </tr>";
            body = body + @"	                                                                <tr>";
            body = body + @"	                                                                    <td valign='top' class='userData'>";
            body = body + @"	                                                                        <p><strong>Contraseña</strong></p>";
            body = body + @"	                                                                        <p>" + txt_password + "</p>";
            body = body + @"	                                                                    </td>";
            body = body + @"	                                                                    <td valign='top' class='userData'>";
            body = body + @"	                                                                        <p><strong>DNI</strong></p>";
            body = body + @"	                                                                        <p>" + nro_doc + "</p>";
            body = body + @"	                                                                    </td>";
            body = body + @"	                                                                </tr>";
            body = body + @"	                                                                <tr>";
            body = body + @"	                                                                    <td valign='top' class='userData'>";
            body = body + @"	                                                                        <p><strong>Nombre</strong></p>";
            body = body + @"	                                                                        <p>" + txt_nombre + "</p>";
            body = body + @"	                                                                    </td>";
            body = body + @"	                                                                    <td valign='top' class='userData'>";
            body = body + @"	                                                                        <p><strong>Apellido</strong></p>";
            body = body + @"	                                                                        <p>" + txt_apellido + "</p>";
            body = body + @"	                                                                    </td>";
            body = body + @"	                                                                </tr>";
            //body = body + @"	                                                                <tr>";
            //body = body + @"	                                                                    <td valign='top' class='userData'>";
            //body = body + @"	                                                                        <p><strong>Agencia</strong></p>";
            //body = body + @"	                                                                        <p>" + agencia + "</p>";
            //body = body + @"	                                                                    </td>";
            //body = body + @"	                                                                    <td valign='top' class='userData'>";
            //body = body + @"	                                                                        <p><strong>Rol</strong></p>";
            //body = body + @"	                                                                        <p>" + rol + "</p>";
            //body = body + @"	                                                                    </td>";
            //body = body + @"	                                                                </tr>";

            body = body + @"	                                                            </table>";
            body = body + @"	                                                        </td>";
            body = body + @"	                                                    </tr>";
            body = body + @"	                                                </table>";
            body = body + @"	                                                <!-- // END BODY -->";
            body = body + @"	                                            </td>";
            body = body + @"	                                        </tr>";
            body = body + @"	                                    </table>";
            body = body + @"	                                </td>";
            body = body + @"	                            </tr>";
            body = body + @"	                            <tr>";
            body = body + @"	                                <td align='center' valign='top'>";
            body = body + @"	                                    <!-- BEGIN FOOTER // -->";
            body = body + @"	                                    <table border='0' cellpadding='0' cellspacing='0' width='100%' id='templateFooter'>";
            body = body + @"	                                        <tr>";
            body = body + @"	                                            <td valign='top' class='footerContent'>";
            body = body + @"	                                                Copyright &copy; 2015 - Sidepro";
            body = body + @"	                                            </td>";
            body = body + @"	                                        </tr>";
            body = body + @"	                                    </table>";
            body = body + @"	                                    <!-- // END FOOTER -->";
            body = body + @"	                                </td>";
            body = body + @"	                            </tr>";
            body = body + @"	                        </table>";
            body = body + @"	                        <!-- // END TEMPLATE -->";
            body = body + @"	                    </td>";
            body = body + @"	                </tr>";
            body = body + @"	            </table>";
            body = body + @"	        </center>";
            body = body + @"	    </body>";
            body = body + @"	</html>";
            #endregion
            mail.Body = body;

            mail.BodyEncoding = System.Text.Encoding.UTF8;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "s184-168-147-58.secureserver.net";
            smtp.Port = 25;
            smtp.Credentials = new NetworkCredential("no-reply@sidepro.com.ar", "Agus221#");
            smtp.EnableSsl = false;
            smtp.Send(mail);
        }
    }
}