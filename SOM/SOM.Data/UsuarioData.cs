﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SOM.Entities;
using System.Data.OleDb;
namespace SOM.Data
{
    public class UsuarioData
    {
        public List<Usuario> getUsuarios()
        {
            List<Usuario> miLista = new List<Usuario>();
            Usuario miUsuario;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_all_users");
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {

                    miUsuario = new Usuario();
                    miUsuario.Id_usuario = Convert.ToInt32(miDataReader["cod_usuario"]);
                    miUsuario.User = miDataReader["txt_user"].ToString();
                    miUsuario.Sn_activo = Convert.ToInt32(miDataReader["sn_activo"]);
                    miUsuario.Nombre = miDataReader["txt_nombre"].ToString();
                    miUsuario.Apellido = miDataReader["txt_apellido"].ToString();
                    miUsuario.Password = miDataReader["Password"].ToString();
                    miUsuario.Id_agencia = Convert.ToInt32(miDataReader["id_agencia"]);
                    miUsuario.Documento = miDataReader["nro_doc"].ToString();
                    miUsuario.Email = miDataReader["txt_email"].ToString();
                    miUsuario.Cod_rol = Convert.ToInt32(miDataReader["cod_rol"]);
                    miUsuario.Cod_estado = Convert.ToInt32(miDataReader["cod_estado"]);
                    miUsuario.Rol = miDataReader["rol"].ToString();
                    miUsuario.Agencia = miDataReader["agencia"].ToString();
                    miLista.Add(miUsuario);
                }
            }
            miDataReader.Close();
            miConn.Close();
            
            return miLista;
        }
        public string cambiarEstado(int id_usuario, int cod_estado)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[2];
                OleDbParameter miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_usuario";
                miParametro.Value = id_usuario;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_estado";
                miParametro.Value = cod_estado;
                misParametros[1] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_cambiar_estado_usuario", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miDataReader.Close();
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string modificarUsuario(int id_usuario, string nombre, string apellido, string password, string email, string dni, int sn_activo, int cod_rol, int id_agencia)
        {
            string Respuesta = "";
            try
            {
                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[9];
                OleDbParameter miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_usuario";
                miParametro.Value = id_usuario;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = nombre;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_apellido";
                miParametro.Value = apellido;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_password";
                miParametro.Value = password;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_email";
                miParametro.Value = email;
                misParametros[4] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@nro_doc";
                miParametro.Value = dni;
                misParametros[5] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_agencia";
                miParametro.Value = id_agencia;
                misParametros[6] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_rol";
                miParametro.Value = cod_rol;
                misParametros[7] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@sn_activo";
                miParametro.Value = sn_activo;
                misParametros[8] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_upd_user_ABM", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miDataReader.Close();
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
            
        }
        public string agregarUsuario(string txt_user, string nombre, string apellido, string password, string email, string dni, int cod_rol, int id_agencia)
        {
            string Respuesta = "";

            try
            {
                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[12];
                OleDbParameter miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_user";
                miParametro.Value = txt_user;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = nombre;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_apellido";
                miParametro.Value = apellido;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_password";
                miParametro.Value = password;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_email";
                miParametro.Value = email;
                misParametros[4] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@nro_doc";
                miParametro.Value = dni;
                misParametros[5] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_agencia";
                miParametro.Value = id_agencia;
                misParametros[6] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_estado";
                miParametro.Value = 0;
                misParametros[7] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@sn_activo";
                miParametro.Value = -1;
                misParametros[8] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cant_intentos";
                miParametro.Value = 0;
                misParametros[9] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_rol";
                miParametro.Value = cod_rol;
                misParametros[10] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@url_imagen";
                miParametro.Value = "";
                misParametros[11] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_ins_usuario_ABM", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miDataReader.Close();
                miConn.Close();

                Helpers.HelpersData.sendEmailAltas(email, txt_user, nombre, apellido, password, dni);
                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public Usuario registrarUsuario(string txt_user, string nombre, string apellido, string password, string email, string dni, int delegacion)
        {
            string Respuesta = "";

            try
            {
                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[13];
                OleDbParameter miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_user";
                miParametro.Value = txt_user;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = nombre;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_apellido";
                miParametro.Value = apellido;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_password";
                miParametro.Value = password;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_email";
                miParametro.Value = email;
                misParametros[4] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@nro_doc";
                miParametro.Value = dni;
                misParametros[5] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_agencia";
                miParametro.Value = 1;
                misParametros[6] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_estado";
                miParametro.Value = 0;
                misParametros[7] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@sn_activo";
                miParametro.Value = -1;
                misParametros[8] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cant_intentos";
                miParametro.Value = 0;
                misParametros[9] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_rol";
                miParametro.Value = 0;
                misParametros[10] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@url_imagen";
                miParametro.Value = "";
                misParametros[11] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_delegacion";
                miParametro.Value = delegacion;
                misParametros[12] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_ins_usuario", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miDataReader.Close();
                miConn.Close();

                if (Respuesta != "")
                {
                    Usuario miUser = new Usuario();
                    miUser.Mensaje = Respuesta;
                    return miUser;
                }else{
                    return new HomeData().getUser(email, password);
                }
            }
            catch (Exception ex)
            {
                Usuario miUser = new Usuario();
                miUser.Mensaje = ex.Message;
                return miUser;

            }
 
        }
    }
}