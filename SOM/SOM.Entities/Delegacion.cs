﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SOM.Entities
{
    public class Delegacion
    {
        private int _Codigo;
        private string _Descripcion;

        public string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }
        public int Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }
    }
}