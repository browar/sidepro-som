﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SOM.Entities
{
    public class Rol
    {
        private int _ID_Rol;

        public int ID_Rol
        {
            get { return _ID_Rol; }
            set { _ID_Rol = value; }
        }
        private string _Nombre;

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
    }
}