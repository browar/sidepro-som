﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SOM.Entities
{
    public class Usuario
    {
        private string _Password;

        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
        private int _Sn_activo;

        public int Sn_activo
        {
            get { return _Sn_activo; }
            set { _Sn_activo = value; }
        }
        private string _User;

        public string User
        {
            get { return _User; }
            set { _User = value; }
        }
        private string _Documento;

        public string Documento
        {
            get { return _Documento; }
            set { _Documento = value; }
        }
        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        private int _Cod_rol;

        public int Cod_rol
        {
            get { return _Cod_rol; }
            set { _Cod_rol = value; }
        }
        private int _Cod_estado;

        public int Cod_estado
        {
            get { return _Cod_estado; }
            set { _Cod_estado = value; }
        }
        private string _Rol;

        public string Rol
        {
            get { return _Rol; }
            set { _Rol = value; }
        }
        private string _Agencia;

        public string Agencia
        {
            get { return _Agencia; }
            set { _Agencia = value; }
        }

        private int _Id_usuario;
        private string _Nombre;
        private string _Apellido;
        private string _URL_imagen;
        private int _cod_tipo_usuario;
        private string _Mensaje;
        private int _Id_agencia;

        public int Id_agencia
        {
            get { return _Id_agencia; }
            set { _Id_agencia = value; }
        }
        public string Mensaje
        {
            get { return _Mensaje; }
            set { _Mensaje = value; }
        }
        public int Cod_tipo_usuario
        {
            get { return _cod_tipo_usuario; }
            set { _cod_tipo_usuario = value; }
        }
        public string URL_imagen
        {
            get { return _URL_imagen; }
            set { _URL_imagen = value; }
        }
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        public string Apellido
        {
            get { return _Apellido; }
            set { _Apellido = value; }
        }
        public int Id_usuario
        {
            get { return _Id_usuario; }
            set { _Id_usuario = value; }
        }



        
    }
}