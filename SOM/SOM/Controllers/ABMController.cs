﻿using SOM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SOM.Application;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using System.IO;
using ClosedXML.Excel;
namespace SOM.Controllers
{
    public class ABMController : Controller
    {
        //
        // GET: /ABM/
        ModuloCursosManagementService moduloCursosManagementService = new ModuloCursosManagementService();
        CursosManagementService cursosManagementService = new CursosManagementService();
        LeccionManagementService leccionManagementService = new LeccionManagementService();
        AgenciaManagementService agenciaManagementService = new AgenciaManagementService();
        RolManagementService rolManagementService = new RolManagementService();
        UsuarioManagementService usuarioManagementService = new UsuarioManagementService();
        public ActionResult Modulo()
        {
            return View( new indexViewModel());
        }
        public ActionResult Usuario()
        {
            return View(new indexViewModel());
        }
        public ActionResult Curso(int id_modulo, int? id_curso)
        {
            var miViewModel = new ABMViewModelController().getABMViewModel(id_modulo, id_curso);
            return View(miViewModel);
        }
        public ActionResult Leccion(int id_modulo, int id_curso)
        {
            var miViewModel = new ABMViewModelController().getLessonABMViewModel(id_modulo, id_curso);
            return View(miViewModel);
        }
        public ActionResult Pregunta(int id_modulo, int id_curso, int id_leccion)
        {
            var miViewModel = new ABMViewModelController().getQuestionABMViewModel(id_modulo, id_curso, id_leccion);
            return View(miViewModel);
        }
        public ActionResult Certificacion(int id_modulo, int id_curso, int id_leccion)
        {
            var miViewModel = new ABMViewModelController().getQuestionABMViewModel(id_modulo, id_curso, id_leccion);
            return View(miViewModel);
        }
        public ActionResult Respuesta(int id_modulo, int id_curso, int id_leccion, int id_pregunta)
        {
            var miViewModel = new ABMViewModelController().getAnswerABMViewModel(id_modulo, id_curso, id_leccion, id_pregunta);
            return View(miViewModel);
        }
        public ActionResult Agencia()
        {
            return View(new indexViewModel());
        }
        public ActionResult Rol()
        {
            return View(new indexViewModel());
        }
        public ActionResult RolModulo(int id_rol)
        {
            RolModuleViewModel miViewModel = new ABMViewModelController().getRolModuleViewModel(id_rol);
            return View(miViewModel);
        }
        public ActionResult RolCurso(int id_rol, int id_modulo, int? id_curso)
        {
            var miViewModel = new ABMViewModelController().getRolCourseViewModel(id_rol, id_modulo, id_curso);
            return View(miViewModel);
        }
        public ActionResult destacado()
        {
            return View(new indexViewModel());
        }
        public ActionResult subirArchivos()
        {
            return View(new indexViewModel());
        }
        #region Modulos
            public ActionResult GetModulos()
            {
                //ModuloCursosManagementService moduloCursosManagementService = new ModuloCursosManagementService();
                var misModulos = moduloCursosManagementService.getModulos();

                return Json(new { modulos = misModulos }, JsonRequestBehavior.AllowGet);

            }
            public ActionResult AgregarModulo(string Nombre, string Controlador, string View, string Style){
                
                string Tags = "";
                string Mensaje = moduloCursosManagementService.agregarModulo(Nombre, Controlador, View, Style, Tags);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarModulo(int ID_Modulo, string Nombre, string Controlador, string View, string Style)
            {
                string Tags = "";
                string Mensaje = moduloCursosManagementService.modificarModulo(ID_Modulo, Nombre, Controlador, View, Style, Tags);
                return Json(new { Mensaje = Mensaje  }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult InhabilitarModulo(int ID_Modulo)
            {
                string Mensaje = moduloCursosManagementService.inhabilitarModulo(ID_Modulo);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult HabilitarModulo(int ID_Modulo)
            {
                string Mensaje = moduloCursosManagementService.habilitarModulo(ID_Modulo);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
        #endregion
        #region Cursos
            public ActionResult GetCursos(int id_modulo, int? id_curso)
            {
                //ModuloCursosManagementService moduloCursosManagementService = new ModuloCursosManagementService();
                var misCursos = cursosManagementService.getCursos(id_modulo, id_curso);

                return Json(new { cursos = misCursos }, JsonRequestBehavior.AllowGet);

            }
            public ActionResult AgregarCurso(int Id_modulo, string Nombre, string Descripcion, string Controlador, string View, string Style,string Imagen, int? Parent)
            {

                string Mensaje = cursosManagementService.agregarCurso(Id_modulo, Nombre, Descripcion, Controlador, View, Style, Imagen, Parent);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarCurso(int ID_Modulo, int Id_curso, string Nombre, string Descripcion, string Controlador, string View, string Style, string Imagen)
            {

                string Mensaje = cursosManagementService.modificarCurso(ID_Modulo, Id_curso, Nombre, Descripcion, Controlador, View, Style, Imagen);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult InhabilitarCurso(int ID_Modulo, int Id_curso)
            {
                string Mensaje = cursosManagementService.InhabilitarCurso(ID_Modulo, Id_curso);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult HabilitarCurso(int ID_Modulo, int Id_curso)
            {
                string Mensaje = cursosManagementService.HabilitarCurso(ID_Modulo, Id_curso);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            
        #endregion
        #region Lecciones
            public ActionResult GetLeccion(int id_modulo, int id_curso)
            {
                //ModuloCursosManagementService moduloCursosManagementService = new ModuloCursosManagementService();
                var misLecciones = leccionManagementService.getLecciones(id_modulo, id_curso);

                return Json(new { lecciones = misLecciones }, JsonRequestBehavior.AllowGet);

            }
            public ActionResult AgregarLeccion(int Id_modulo, int Id_curso, string Nombre, string Descripcion, string Imagen, string Video, decimal Pje_aprobacion)
            {
                string Mensaje = leccionManagementService.AgregarLeccion(Id_modulo, Id_curso, Nombre,  Descripcion,  Imagen,  Video,  Pje_aprobacion);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarLeccion(int Id_modulo, int Id_curso, int Id_leccion, string Nombre, string Descripcion, string Imagen, string Video, decimal Pje_aprobacion, int? Orden)
            {
                string Mensaje = leccionManagementService.ModificarLeccion(Id_modulo, Id_curso, Id_leccion, Nombre, Descripcion, Imagen, Video, Pje_aprobacion, Orden);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult InhabilitarLeccion(int ID_Modulo, int Id_curso, int Id_leccion)
            {
                string Mensaje = leccionManagementService.InhabilitarLeccion(ID_Modulo, Id_curso, Id_leccion);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult HabilitarLeccion(int ID_Modulo, int Id_curso, int Id_leccion)
            {
                string Mensaje = leccionManagementService.HabilitarLeccion(ID_Modulo, Id_curso, Id_leccion);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }

        #endregion 
        #region Pregunta

            public ActionResult GetPregunta(int id_modulo, int id_curso, int Id_leccion)
            {

                var misPreguntas = leccionManagementService.GetPregunta(id_modulo, id_curso, Id_leccion);
                return Json(new { preguntas = misPreguntas }, JsonRequestBehavior.AllowGet);

            }
            public ActionResult AgregarPregunta(int Id_modulo, int Id_curso, int Id_leccion, string Nombre, string Correcto, string Incorrecto)
            {
                string Mensaje = leccionManagementService.AgregarPregunta(Id_modulo, Id_curso, Id_leccion, Nombre, Correcto, Incorrecto);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarPregunta(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, string Nombre, string Correcto, string Incorrecto, decimal Cant_puntos)
            {
                string Mensaje = leccionManagementService.ModificarPregunta(Id_modulo, Id_curso, Id_leccion, Id_pregunta, Nombre, Correcto, Incorrecto, Cant_puntos);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }

            public ActionResult InhabilitarPregunta(int ID_Modulo, int Id_curso, int Id_leccion, int Id_pregunta)
            {
                string Mensaje = leccionManagementService.InhabilitarPregunta(ID_Modulo, Id_curso, Id_leccion, Id_pregunta);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult HabilitarPregunta(int ID_Modulo, int Id_curso, int Id_leccion, int Id_pregunta)
            {
                string Mensaje = leccionManagementService.HabilitarPregunta(ID_Modulo, Id_curso, Id_leccion, Id_pregunta);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarPregCertificacion (int Id_modulo, int Id_curso, int Id_leccion, string Nombre, string Correcto, string Incorrecto, int categoria)
            {
                string Mensaje = leccionManagementService.AgregarPreguntaCertificacion(Id_modulo, Id_curso, Id_leccion, Nombre, Correcto, Incorrecto, categoria);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarPregCertificacion(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, string Nombre, string Correcto, string Incorrecto, decimal Cant_puntos, int categoria)
            {
                string Mensaje = leccionManagementService.ModificarPreguntaCertificacion(Id_modulo, Id_curso, Id_leccion, Id_pregunta, Nombre, Correcto, Incorrecto, Cant_puntos,categoria);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
        #endregion
        #region Respuesta
            public ActionResult GetRespuesta(int id_modulo, int id_curso, int Id_leccion, int id_pregunta)
            {
                var misRespuestas = leccionManagementService.GetRespuesta(id_modulo, id_curso, Id_leccion, id_pregunta);
                return Json(new { respuestas = misRespuestas }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarRespuesta(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, string Nombre, int Sn_correcto)
            {
                string Mensaje = leccionManagementService.AgregarRespuesta(Id_modulo, Id_curso, Id_leccion, Id_pregunta, Nombre, Sn_correcto);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarRespuesta(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, int id_respuesta, string Nombre, int Sn_correcto)
            {
                string Mensaje = leccionManagementService.ModificarRespuesta(Id_modulo, Id_curso, Id_leccion, Id_pregunta, id_respuesta, Nombre, Sn_correcto);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }

            public ActionResult InhabilitarRespuesta(int ID_Modulo, int Id_curso, int Id_leccion, int Id_pregunta, int id_respuesta)
            {
                string Mensaje = leccionManagementService.InhabilitarRespuesta(ID_Modulo, Id_curso, Id_leccion, Id_pregunta, id_respuesta);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult HabilitarRespuesta(int ID_Modulo, int Id_curso, int Id_leccion, int Id_pregunta, int id_respuesta)
            {
                string Mensaje = leccionManagementService.HabilitarRespuesta(ID_Modulo, Id_curso, Id_leccion, Id_pregunta, id_respuesta);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
        #endregion
        #region Agencias
            public ActionResult GetAgencias()
            {
                var misAgencias = agenciaManagementService.getAgencias();
                return Json(new { Agencias = misAgencias }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult GetTipoAgencias()
            {
                var tipoAgencias = agenciaManagementService.getTipoAgencias();
                return Json(new { TipoAgencias = tipoAgencias }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarAgencia(string Agencia, string Documento, string Email, int TipoAgencia)
            {

                string Mensaje = agenciaManagementService.agregarAgencia(Agencia, Documento, Email, TipoAgencia);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarAgencia(int id_agencia, string Agencia, string Documento, string Email, int TipoAgencia)
            {
                string Mensaje = agenciaManagementService.modificarAgencia(id_agencia, Agencia, Documento, Email, TipoAgencia);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }

        #endregion
        #region Roles
            public ActionResult GetRoles()
            {
                var misRoles = rolManagementService.getRoles();
                return Json(new { Roles = misRoles }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarRol(string Rol)
            {
                string Mensaje = rolManagementService.agregarRol(Rol);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarRol(int id_rol, string Rol)
            {
                string Mensaje = rolManagementService.modificarRol(id_rol, Rol);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }

        #endregion
        #region RolesModulos

            public ActionResult GetRolModulos(int id_rol)
            {
                var misModulos = rolManagementService.getRolModulos(id_rol);
                return Json(new { Modulos = misModulos }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult GetModulosABM(int id_rol)
            {
                var misModulos = rolManagementService.getModulos(id_rol);
                return Json(new { Modulos = misModulos }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarRolModulo(int ID_Rol, int ID_Modulo)
            {
                string Mensaje = rolManagementService.agregarRolModulo(ID_Rol, ID_Modulo);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult EliminarRolModulo(int ID_Modulo, int ID_Rol)
            {
                string Mensaje = rolManagementService.eliminarRolModulo(ID_Modulo, ID_Rol);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }


        #endregion
        #region RolesCursos
            public ActionResult GetRolCursos(int id_rol, int id_modulo, int? id_curso)
            {
                var misCursos = rolManagementService.getRolCursos(id_rol, id_modulo, id_curso);
                return Json(new { Cursos = misCursos }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult GetCursosABM(int id_rol, int id_modulo, int? id_curso)
            {
                var misModulos = rolManagementService.getCursos(id_rol, id_modulo, id_curso);
                return Json(new { Cursos = misModulos }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarRolCurso(int ID_Rol, int ID_Modulo, int ID_Curso, int? parent)
            {
                string Mensaje = rolManagementService.agregarRolCurso(ID_Rol, ID_Modulo,ID_Curso,parent);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult EliminarRolCurso(int ID_Modulo, int ID_Curso, int ID_Rol)
            {
                string Mensaje = rolManagementService.eliminarRolCurso(ID_Modulo,ID_Curso, ID_Rol);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }        
        
        #endregion
        #region Destacados
            public ActionResult GetDestacados()
            {
                var misDestacados = rolManagementService.getDestacados();
                return Json(new { Destacados = misDestacados }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult GetRolesDestacado() {
                var misRoles = rolManagementService.getRolesDestacados();
                return Json(new { Roles = misRoles }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult GetModulosDestacado(int cod_rol ) {
                var misModulos = rolManagementService.getModulosDestacados(cod_rol);
                return Json(new { Modulos = misModulos }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult GetCursosDestacado(int cod_rol, int id_modulo, int? id_curso)
            {
                var misCursos  = rolManagementService.getCursosDestacados(cod_rol, id_modulo, id_curso);
                return Json(new { Cursos = misCursos}, JsonRequestBehavior.AllowGet);
            }
            public ActionResult GetTipoDestacados()
            {
                var misDestacados = rolManagementService.getTipoDestacados();
                return Json(new { Destacados = misDestacados }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarDestacado(int id_destacado, int cod_tipo_destacado, string url_imagen, int orden)
            {
                string Mensaje = rolManagementService.ModificarDestacado(id_destacado, cod_tipo_destacado, url_imagen, orden);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarDestacado(int id_modulo, int? id_curso,int cod_rol, int cod_tipo_destacado, string url_imagen)
            {
                string Mensaje = rolManagementService.AgregarDestacado(id_modulo, id_curso, cod_rol, cod_tipo_destacado, url_imagen);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult InhabilitarDestacado(int id_destacado)
            {
                string Mensaje = rolManagementService.EliminarDestacado(id_destacado);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
        #endregion
        #region Archivos

            public bool checkArchivos(string fileName, int tipo_archivo)
            {

                string extension = Path.GetExtension(fileName).Remove(0, 1);// fileName.Split('.')[fileName.Split('.').Length - 1];
                if (tipo_archivo == 3) {
                    if ((extension.ToLower() == "xls") || (extension.ToLower() == "xlsx") || (extension.ToLower() == "csv"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }else{

                    if (tipo_archivo == 1)
                    {
                        if ((extension.ToLower() == "gif") || (extension.ToLower() == "jpg") || (extension.ToLower() == "jpeg") || (extension.ToLower() == "png"))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if ((extension.ToLower() == "mp4") || (extension.ToLower() == "avi") || (extension.ToLower() == "mpeg4"))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            [HttpPost]
            public ActionResult GrabarArchivo()
            {
                int id_modulo = Convert.ToInt32(Request["id_modulo"]);
                int id_curso = Convert.ToInt32(Request["id_curso"]);
                int id_leccion = Convert.ToInt32(Request["id_leccion"]);
                int tipo_archivo = Convert.ToInt32(Request["cod_tipo_archivo"]);


                try
                {
                    foreach (string file in Request.Files)
                    {
                        if (!checkArchivos(Request.Files[file].FileName, tipo_archivo))
                        {
                            return Json(new { Mensaje = "Error, tipo de archivo no válido" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    foreach (string file in Request.Files)
                    {
                        
                        var fileContent = Request.Files[file];
                        if (fileContent != null && fileContent.ContentLength > 0)
                        {
                            // get a stream
                            var stream = fileContent.InputStream;
                            // and optionally write the file to disk
                            var fileName = Path.GetFileName(file);
                            var path = "";


                            string nombre = new HelperManagementService().saveFile(id_modulo, id_curso, id_leccion, tipo_archivo, Request.Files[file].FileName);
                            if (tipo_archivo == 0 ){
                                path = Path.Combine(Server.MapPath("~/Images/Videos/"), nombre);
                            }else{
                                if (id_modulo == 0 ){
                                    path = Path.Combine(Server.MapPath("~/Images/Modulos/Modulo_inicial/"), nombre);
                                }else{
                                    if (id_modulo == 1){
                                        path = Path.Combine(Server.MapPath("~/Images/Modulos/Equipos/"), nombre);
                                    }else{
                                        path = Path.Combine(Server.MapPath("~/Images/Modulos/Ofertas_y_campañas/"), nombre);
                                    }
                                }
                            
                            }
                            
                            using (var fileStream = System.IO.File.Create(path))
                            {
                                stream.CopyTo(fileStream);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { Mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
                }
                
                

                /*
               string path = System.IO.Path.Combine(Server.MapPath("~/Images"), System.IO.Path.GetFileName(file.FileName));
               file.SaveAs(@"C:\testFiles\archivo.txt");
               ViewBag.Message = "File uploaded successfully";
               return RedirectToAction("subirArchivos", "ABM");
                 * */
                return Json(new { Mensaje = "Archivo subido con éxito" }, JsonRequestBehavior.AllowGet);
            }
            
        #endregion

        #region Usuarios
            public ActionResult GetUsuarios()
            {
                //ModuloCursosManagementService moduloCursosManagementService = new ModuloCursosManagementService();
                var misUsuarios = usuarioManagementService.getUsuarios();

                return Json(new { Usuarios = misUsuarios}, JsonRequestBehavior.AllowGet);

            }
            public ActionResult CambiarEstadoUsuario(int id_usuario, int cod_estado) 
            {
                var mensaje = usuarioManagementService.cambiarEstado(id_usuario, cod_estado);

                return Json(new { Mensaje = mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarUsuario(string txt_user, string nombre, string apellido, string password, string email, string dni, int cod_rol, int id_agencia)
            {
                var mensaje = usuarioManagementService.agregarUsuario(txt_user, nombre, apellido, password, email, dni, cod_rol, id_agencia);

                return Json(new { Mensaje = mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarUsuario(int id_usuario, string nombre, string apellido, string password, string email, string dni, int sn_activo, int cod_rol, int id_agencia)
            {
                var mensaje = usuarioManagementService.modificarUsuario(id_usuario, nombre, apellido, password, email, dni, sn_activo, cod_rol, id_agencia);

                return Json(new { Mensaje = mensaje }, JsonRequestBehavior.AllowGet);
            }
            [HttpPost]
            public ActionResult ImportarUsuariosExcel()
            {

                string resultado = "";
                int tipo_archivo = Convert.ToInt32(Request["cod_tipo_archivo"]);
                try
                {
                    foreach (string file in Request.Files)
                    {
                        if (!checkArchivos(Request.Files[file].FileName, tipo_archivo))
                        {
                            return Json(new { Mensaje = "Error, tipo de archivo no válido" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    foreach (string file in Request.Files)
                    {

                        
                        
                        var fileContent = Request.Files[file];
                        if (fileContent != null && fileContent.ContentLength > 0)
                        {
                            
                            // get a stream
                            var stream = fileContent.InputStream;
                            // and optionally write the file to disk
                            var fileName = Path.GetFileName(file);
                            //var path = "";
                            
                            string miNombreArchivo = Guid.NewGuid().ToString();
                            string extension = Path.GetExtension(fileContent.FileName).Remove(0, 1);
                            miNombreArchivo = miNombreArchivo + "." + extension;
                            string path = Path.Combine(Server.MapPath("~/Images/Excel/"), miNombreArchivo);
                            using (var fileStream = System.IO.File.Create(path))
                            {
                                stream.CopyTo(fileStream);
                            }

                            //string path = graboArchivoEnLocal(file);

                            var workbook = new XLWorkbook(path);
                            var ws1 = workbook.Worksheet(1);

                            bool vacio = true ;
                            vacio = ws1.Row(2).IsEmpty();
                            int i = 2;
                            IXLRow miRow;
                            string user;
                            string nombre;
                            string apellido;
                            string password;
                            string email;
                            string documento;
                            int id_agencia;
                            int cod_rol;
                            string mensaje;
                            int cantErrores = 0;
                            while (!vacio)
                            {
                                miRow = ws1.Row(i);
                                user = miRow.Cell(1).GetValue<string>();
                                nombre = miRow.Cell(2).GetValue<string>(); ;
                                apellido = miRow.Cell(3).GetValue<string>(); ;
                                password = miRow.Cell(4).GetValue<string>(); ;
                                email = miRow.Cell(5).GetValue<string>(); ;
                                documento = miRow.Cell(6).GetValue<string>(); ;
                                id_agencia = miRow.Cell(7).GetValue<int>(); ;
                                cod_rol = miRow.Cell(8).GetValue<int>(); ;

                                mensaje = usuarioManagementService.agregarUsuario(user, nombre, apellido, password, email, documento, cod_rol, id_agencia);
                                if (mensaje != "")
                                {

                                    if (cantErrores == 0)
                                    {
                                        resultado = "Error en la linea " + i.ToString() + " - ("+ mensaje +"); ";
                                    }else{
                                        if (cantErrores == 1)
                                        {
                                            resultado = resultado.Replace("Error en la linea ", "Error en las lineas");
                                        }
                                        resultado = resultado + ", " + i.ToString() + " - (" + mensaje + "); ";
                                    }
                                    cantErrores++; 
                                }
                                i++;
                                vacio = ws1.Row(i).IsEmpty();
                            }

                            System.IO.File.Delete(path);
                            if (cantErrores == 0)
                            {
                                resultado = "Usuarios dados de alta con éxito";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { Mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
                }

                

                return Json(new { Mensaje = resultado }, JsonRequestBehavior.AllowGet);
                //return Json(new { Mensaje = "" }, JsonRequestBehavior.AllowGet);
            }
        #endregion


            public int obtenerUsuario()
        {
            int id_usuario = -1;
            if (HttpContext.Session["IDUsuario"] != null)
            {
                id_usuario = (int)HttpContext.Session["IDUsuario"];
            }
            else
            {
                id_usuario = new indexViewModel().id_usuario;
            }
            return id_usuario;
        }

    }
}
