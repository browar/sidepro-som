﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using SOM.Application;
using SOM.ViewModels;
namespace Sidepro.Controllers
{
    public class AjaxController : Controller
    {
        //
        // GET: /Ajax/

        public ActionResult help()
        {
            return View();
        }
        public ActionResult certificacionesAgencia(int id_agencia, int mes, int ano)
        {
            HelperViewModel miViewModel = new HelperViewModelController().getHelperViewModel(id_agencia,mes,ano);
            return View(miViewModel);
        }
        public ActionResult EnviarMailConsulta(string Consulta)
        {
            bool result = true;
            HelperManagementService helperManagementService = new HelperManagementService();
            int cod_usuario = obtenerUsuario();
            result = helperManagementService.enviarMail(cod_usuario, "no-reply@sidepro.com.ar", "mibrowar@gmail.com", "mibrowar@gmail.com", "Testeando Mail", Consulta);
            //string pepe = SendUserMail("no-reply@s184-168-147-58.secureserver.net", "mibrowar@gmail.com", "probando, probando", "no-reply@s184-168-147-58.secureserver.net", "titulo");
            return Json(new { resultado = result}, JsonRequestBehavior.AllowGet);
        }
       
       public string SendUserMail(string fromad, string toad, string body, string header, string subjectcontent)
       {
           string result = "";
           MailMessage usermail = Mailbodplain(fromad, toad, body, header, subjectcontent);
           SmtpClient client = new SmtpClient();
           //Add the Creddentials- use your own email id and password
           client.Credentials = new System.Net.NetworkCredential("amedia", "Agus221#"); ;

           client.Host = "s184-168-147-58.secureserver.net";// "smtp.gmail.com";
           client.Port = 25;
           //client.EnableSsl = true;
           try
           {
               client.Send(usermail);
               result = "todoOk";
           }
           catch (Exception ex)
           {
               result = ex.Message;
           } // end try

           return result;
 
       }
       public MailMessage Mailbodplain(string fromad, string toad, string body, string header, string subjectcontent)
       {
           System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
           try
           {
               string from = fromad;
               string to = toad;
               mail.To.Add(to);
               mail.From = new MailAddress( from, header, System.Text.Encoding.UTF8);
               mail.Subject = subjectcontent;
               mail.SubjectEncoding = System.Text.Encoding.UTF8;
               mail.Body = body;
               mail.BodyEncoding = System.Text.Encoding.UTF8;
               mail.IsBodyHtml = true;
               mail.Priority = MailPriority.High;
           }
           catch
           {
               throw;
           }
           return mail;
       }
       public int obtenerUsuario()
       {
           int id_usuario = -1;
           if (HttpContext.Session["IDUsuario"] != null)
           {
               id_usuario = (int)HttpContext.Session["IDUsuario"];
           }
           else
           {
               id_usuario = new indexViewModel().id_usuario;
           }
           return id_usuario;
       }

    }
}
