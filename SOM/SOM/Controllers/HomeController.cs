﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SOM.Application;
using SOM.ViewModels;
using SOM.Entities;
using System.Web.Script.Serialization;
using System.Net.Mail;
using System.Net;
using System.IO;

namespace Sidepro.Controllers
{
    public class HomeController : Controller
    {
        CursosViewModelController cursosVieweModelController = new CursosViewModelController();
        public ActionResult home()
        {
            int usuario = obtenerUsuario();
            var miViewModel = new HomeViewModelController().getHomeViewModel(usuario);
            return View(miViewModel);
        }
        public ActionResult profile()
        {
            ViewBag.Message = "Página inicial";

            return View(new indexViewModel());
        }
        public ActionResult HistorialCertificacion()
        {
            HomeManagementService miManagementService = new HomeManagementService();
            int id_usuario = obtenerUsuario();
            var misCertificaciones = miManagementService.getCertificaciones(id_usuario);
            var json = new JavaScriptSerializer().Serialize(misCertificaciones);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index3()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }
        public ActionResult index()
        {


            if (Request.Cookies["UserSettings"] != null)
            {
                HttpCookie myCookie = new HttpCookie("UserSettings");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }

            HttpContext.Session["IDUsuario"] = null;
            HttpContext.Session["Menu"] = null;
            HttpContext.Session["Historial"] = null;


            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            return View();

        }
        public ActionResult Register()
        {
            var viewModel = new HomeViewModelController().getRegistroViewModel();
            return View(viewModel);

        }
        public ActionResult RegistrarUsuario(string nombre, string apellido, string password, string dni, string email, int delegacion)
        {
            HomeManagementService miManagementService = new HomeManagementService();

            var miUser = new UsuarioManagementService().registrarUsuario(email,nombre, apellido, password, email,dni, delegacion);
            if (miUser.Mensaje == "") { 
                HttpCookie myCookie = new HttpCookie("UserSettings");
                myCookie["Codigo"] = miUser.Id_usuario.ToString();
                myCookie["Nombre"] = miUser.Nombre;
                myCookie["Apellido"] = miUser.Apellido;
                myCookie["Tipo"] = miUser.Cod_tipo_usuario.ToString();
                myCookie["Rol"] = miUser.Cod_tipo_usuario.ToString();
                myCookie["Agencia"] = miUser.Id_agencia.ToString();
                myCookie.Expires = DateTime.Now.AddDays(1d);

                Response.Cookies.Add(myCookie);
                HttpContext.Session["IDUsuario"] = miUser.Id_usuario;
                HttpContext.Session["Menu"] = null;
                HttpContext.Session["Historial"] = null;
                HttpContext.Session["Tipo"] = miUser.Cod_tipo_usuario;
                HttpContext.Session["Agencia"] = miUser.Id_agencia;
            }
            return Json(new { login = (miUser.Mensaje == ""), tipo_usuario = miUser.Cod_tipo_usuario, mensaje = miUser.Mensaje }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Login(string user, string pass)
        {

            HomeManagementService miManagementService = new HomeManagementService();

            Usuario miUser = miManagementService.getUser(user, pass);
            HttpCookie myCookie = new HttpCookie("UserSettings");
            myCookie["Codigo"] = miUser.Id_usuario.ToString();//"1";
            myCookie["Nombre"] = miUser.Nombre;// "Eddard";
            myCookie["Apellido"] = miUser.Apellido;// "Stark Cook";
            myCookie["Tipo"] = miUser.Cod_tipo_usuario.ToString() ;// "Stark Cook";
            myCookie["Rol"] = miUser.Cod_tipo_usuario.ToString();// "Stark Cook";
            myCookie["Agencia"] = miUser.Id_agencia.ToString();// "Stark Cook";
            myCookie.Expires = DateTime.Now.AddDays(1d);

            Response.Cookies.Add(myCookie);
            HttpContext.Session["IDUsuario"] = miUser.Id_usuario;
            HttpContext.Session["Menu"] = null;
            HttpContext.Session["Historial"] = null;
            HttpContext.Session["NombreUsuario"] = miUser.Nombre;
            HttpContext.Session["ApellidoUsuario"] = miUser.Apellido;
            HttpContext.Session["Tipo"] = miUser.Cod_tipo_usuario;
            HttpContext.Session["Agencia"] = miUser.Id_agencia;

            return Json(new { login = (miUser.Mensaje == ""), tipo_usuario = miUser.Cod_tipo_usuario, mensaje = miUser.Mensaje }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult CambiarPassword(string pass_actual, string pass_nueva) {
            HomeManagementService miManagementService = new HomeManagementService();
            int id_usuario = obtenerUsuario();
            var msg = miManagementService.CambiarPassword(id_usuario, pass_actual, pass_nueva);
            return Json(new { Mensaje = msg}, JsonRequestBehavior.AllowGet);
        }
        public int obtenerUsuario()
        {
            int id_usuario = -1;
            if (HttpContext.Session["IDUsuario"] != null)
            {
                id_usuario = (int)HttpContext.Session["IDUsuario"];
            }
            else
            {
                id_usuario = new indexViewModel().id_usuario;
            }
            return id_usuario;
        }

        public ActionResult recuperarPassword(string Email)
        {

            var miMsg = new HomeManagementService().recuperarPassword(Email);
            if (miMsg.Mensaje == ""){
                string rta = gmail_send(Email, miMsg.Nombre, miMsg.Apellido, miMsg.Password); ;//sendEmailReseteoPassword(miMsg.Email, miMsg.Nombre, miMsg.Apellido, miMsg.Password);
                if (rta != "")
                {
                    miMsg.Mensaje = rta;
                }
            }
            return Json(new { Mensaje = miMsg.Mensaje}, JsonRequestBehavior.AllowGet);
        }
        public static string sendEmailReseteoPassword(string Email, string Nombre, string Apellido, string Password)
        {
            try
            {
                MailAddress to = new MailAddress("mibrowar@gmail.com"); // new MailAddress(Console.ReadLine());
                MailAddress from = new MailAddress("no-reply@sidepro.com.ar");//new MailAddress("noreplysidepro@gmail.com");
                MailMessage mail = new MailMessage(from, to);
                mail.IsBodyHtml = true;

                /*
                List<MailAddress> misCopias = obtenerCopiasAgencias(cod_usuario);// new List<MailAddress>();
                misCopias.Add(new MailAddress("no-reply@sidepro.com.ar"));
                foreach (MailAddress miAdress in misCopias)
                {
                    mail.Bcc.Add(miAdress);
                }
                */
                mail.Bcc.Add(new MailAddress("martin.browarnik@amedia.com.ar"));
                mail.Bcc.Add(new MailAddress("mibroawr@hotmail.com"));

                mail.Subject = "Recuperar password SOM";

                string body = "";
                body = body + @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>";
                body = body + @"<html xmlns='http://www.w3.org/1999/xhtml'>";
                body = body + @"<head>";
                body = body + @"<title>Recupera tu contraseña</title>";
                body = body + @"<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
                body = body + @"<meta name='viewport' content='width=device-width, initial-scale=1.0' />";
                body = body + @"<style type='text/css'>*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%}a{outline:0;color:#fda50c;text-decoration:none}a:hover{text-decoration:underline!important}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important}.link a{color:#fff;text-decoration:underline}.link a:hover{text-decoration:none!important}.active:hover{opacity:.8}.active{-webkit-transition:all .3s ease;-moz-transition:all .3s ease;-ms-transition:all .3s ease;transition:all .3s ease}.footer a{color:#fff}table td{border-collapse:collapse!important;mso-line-height-rule:exactly}.ExternalClass,.ExternalClass a,.ExternalClass span,.ExternalClass b,.ExternalClass br,.ExternalClass p,.ExternalClass div{line-height:inherit}@media only screen and (max-width:500px){table[class='flexible']{width:100%!important}*[class='hide']{display:none!important;width:0!important;height:0!important;padding:0!important;font-size:0!important;line-height:0!important}td[class='img-flex'] img{width:100%!important;height:auto!important}td[class='aligncenter']{text-align:center!important}th[class='flex']{display:block!important;width:100%!important}tr[class='table-holder']{display:table!important;width:100%!important}th[class='thead']{display:table-header-group!important;width:100%!important}th[class='tfoot']{display:table-footer-group!important;width:100%!important}td[class='wrapper']{padding:5px!important}td[class='header']{padding:30px 10px!important}td[class='header'] *{text-align:center!important}td[class='title']{padding:0 0 30px!important;font-size:30px!important;line-height:36px!important}td[class='frame']{padding:20px 10px!important}td[class='holder']{padding:20px 20px 30px!important}td[class='holder'] *{text-align:center!important}td[class='block']{padding:0 10px 10px!important}td[class='box']{padding:0 20px 20px!important}td[class='nav']{line-height:30px!important}td[class='p-30']{padding:0 0 30px!important}td[class='p-20']{padding:0 0 20px!important}td[class='p-10']{padding:0 0 10px!important}td[class='block-10']{padding:0 10px 20px!important}td[class='indent-null']{padding:0!important}td[class='indent']{padding:0 10px!important}td[class='w-20']{width:20px!important}td[class='h-auto']{height:auto!important}td[class='footer']{padding:20px 10px!important}}</style>";
                body = body + @"</head>";
                body = body + @"<body style='margin:0;padding:0'><table style='min-width:320px' width='100%' cellspacing='0' cellpadding='0'><tr><td style='line-height:0'><div style='display:none;white-space:nowrap;font:15px/1px courier'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td></tr><tr><td class='hide'><table width='700' cellpadding='0' cellspacing='0' style='width:700px!important'><tr><td style='min-width:700px;font-size:0;line-height:0'>&nbsp;</td></tr></table></td></tr></table><table style='min-width:320px' width='100%' cellspacing='0' cellpadding='0'><tr><td><table data-module='pre-header' width='100%' cellpadding='0' cellspacing='0' style='display:none'><tr><td data-bgcolor='bg-module' bgcolor='#32862B' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td data-color='text-pre-header' data-size='size text-pre-header' data-min='10' data-max='24' data-link-color='link text-pre-header color' data-link-style='text-decoration:none; color:#fff;' align='center' style='padding:9px 10px 11px;font:14px/19px Arial,Helvetica,sans-serif;color:#83d27d'></td></tr></table></td></tr></table><table data-module='header' width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-module' bgcolor='#4EB046' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td class='header' style='padding:38px 0 37px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><th class='flex' align='center' style='vertical-align:top;padding:0'><table width='100%' cellpadding='0' cellspacing='0'><tr><td><a href='#' style='text-decoration:none;color:#fff;display:block;width:78px;margin:auto'><img src='https://sidepro.com.ar/som/images/som-logo-header2.png' height='121' width='78' border='0' style='vertical-align:top' alt='SOM' /></a></td></tr></table></th></tr></table></td></tr></table></td></tr></table><table data-module='module-1' width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-module' bgcolor='#4EB046' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td class='img-flex'><a href='#'><img src='https://sidepro.com.ar/som/images/imgMail.jpg' height='318' width='700' border='0' style='vertical-align:top' /></a></td></tr></table></td></tr></table><table data-module='module-2' width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-module' bgcolor='#4EB046' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td class='frame' bgcolor='#ffffff' style='padding:28px 40px 20px'><table width='100%' cellpadding='0' cellspacing='0'><tr><td data-color='title' data-size='size title' data-min='20' data-max='40' data-link-color='link title color' data-link-style='text-decoration:underline; color:#000;' align='center' style='padding:0 0 15px;font:bold 24px/26px Arial,Helvetica,sans-serif;color:#000'>¡Hola " + Nombre + " " + Apellido + "!</td></tr><tr><td data-color='text' data-size='size text' data-min='10' data-max='26' data-link-color='link text color' data-link-style='text-decoration:underline; color:#000;' style='font:14px/25px Arial,Helvetica,sans-serif;color:#000'>Solicitaste que se enviara este email para recuperar tu contraseña. <br />Para ingresar deberas utilizar la siguiente contraseña y cambiarla una vez que hayas ingresado a la plataforma.</td></tr></table></td></tr></table></td></tr></table><table data-module='module-3' width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-module' class='block-10' bgcolor='#4EB046' style='padding:0 10px 50px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-block' class='frame' bgcolor='#ffffff' style='border-radius:0 0 3px 3px;padding:20px 40px 40px'><table width='100%' cellpadding='0' cellspacing='0'><tr><td><table width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-content' class='holder' bgcolor='#32862B' style='padding:36px 39px 40px;border-radius:3px'><table width='100%' cellpadding='0' cellspacing='0'><tr class='link'><td data-color='title' data-size='size title' data-min='20' data-max='40' data-link-color='link title color' data-link-style='text-decoration:underline; color:#fff;' align='center' style='padding:0 0 15px;font:bold 24px/26px Arial,Helvetica,sans-serif;color:#fff'>Tu contraseña</td></tr><tr class='link'><td data-color='title' data-size='size title' data-min='20' data-max='40' data-link-color='link title color' data-link-style='text-decoration:underline; color:#fff;' align='center' style='padding:0 0 15px;font:18px/20px Arial,Helvetica,sans-serif;color:#fff'>" + Password + "</td></tr><tr class='link' align='center'><td data-color='text' data-size='size text' data-min='10' data-max='26' data-link-color='link text color' data-link-style='text-decoration:underline; color:#fff;' style='padding:0 0 23px;font:14px/25px Arial,Helvetica,sans-serif;color:#fff'>Recuerda cambiarla cuando ingreses a la plataforma</td></tr><tr><td><table align='center' cellpadding='0' cellspacing='0' style='margin:0 auto'><tr><td data-bgcolor='bg-button' data-size='size button' data-min='10' data-max='26' class='active' bgcolor='#ffffff' align='center' style='mso-padding-alt:13px 20px 11px;border-radius:3px;font:bold 15px/17px Arial,Helvetica,sans-serif;color:#fda50c;text-transform:uppercase'><a href='http://sidepro.com.ar/som' style='text-decoration:none!important;color:#32862B!important;display:block;padding:13px 20px 11px'>Quiero ingresar</a></td></tr></table></td></tr></table></td></tr></table></td></tr></table></td></tr></table></td></tr></table><table data-module='footer' width='100%' cellpadding='0' cellspacing='0' style='display:none'><tr><td data-bgcolor='bg-module' bgcolor='#32862B' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td class='footer' style='padding:30px 20px'><table width='100%' cellpadding='0' cellspacing='0'><tr><td class='p-20' style='padding:0 0 20px'><table align='center' cellpadding='0' cellspacing='0' style='margin:0 auto'><tr><td><a class='active' href='https://www.facebook.com/Sindicato-de-Obreros-de-Maestranza-143264025813304'><img src='sidepro.com.ar/som/Images/ico-facebook.png' height='17' width='9' border='0' style='vertical-align:top' alt='facebook' /></a></td><td class='w-20' width='29'></td><td><a class='active' href='https://twitter.com/somaestranza'><img src='sidepro.com.ar/som/images/ico-twitter.png' height='15' width='19' border='0' style='vertical-align:top' alt='twitter' /></a></td></tr></table></td></tr><tr><td data-color='text' data-size='size text' data-min='10' data-max='26' data-link-color='link text color' data-link-style='text-decoration:none; color:#fff;' align='center' style='font:14px/25px Arial,Helvetica,sans-serif;color:#fff'><a href='http://www.som.org.ar'>www.som.org.ar</a></td></tr></table></td></tr></table></td></tr></table></td></tr></table></body></html>";
                mail.Body = body;
                mail.BodyEncoding = System.Text.Encoding.UTF8;

                SmtpClient smtp = new SmtpClient();
                //smtp.SendCompleted += new SendCompletedEventHandler(smtp_SendCompleted);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Host = "s184-168-147-58.secureserver.net";
                smtp.Port = 25;//25;
                smtp.Credentials = new NetworkCredential("no-reply@sidepro.com.ar", "Agus221#");
                smtp.EnableSsl = false;
                smtp.Send(mail);
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public void smtp_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Cancelled == true || e.Error != null)
            {
                throw new Exception(e.Cancelled ? "EMail sedning was canceled." : "Error: " + e.Error.ToString());
            }
        }
        public static string testeoBat(string Email, string Nombre, string Apellido, string Password)
        {

            try
            { 
                //string strFilePath = @"C:\\Users\\amedia\\Desktop\\EnviarMailsCambioPasswordSidepro\\testeoMailsBat.exe " + Email + " " + Nombre + " " + Apellido + " " + Password;
                string strFilePath = @"C:\Users\Browar\Desktop\EnviarMailsCambioPasswordSidepro\\testeoMailsBat.exe ";// +Email + " " + Nombre + " " + Apellido + " " + Password;
                // Create the ProcessInfo object
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo("cmd.exe");
                psi.UseShellExecute = false; 
                psi.RedirectStandardOutput = true;
                psi.RedirectStandardInput = true;
                psi.RedirectStandardError = true;
                //psi.WorkingDirectory = @"C:\\Users\\amedia\\Desktop\\EnviarMailsCambioPasswordSidepro";
                psi.WorkingDirectory = @"C:\Users\Browar\Desktop\EnviarMailsCambioPasswordSidepro";

                // Start the process
                System.Diagnostics.Process proc = System.Diagnostics.Process.Start(psi);
                // Open the batch file for reading
                System.IO.StreamReader strm = System.IO.File.OpenText(strFilePath);
                // Attach the output for reading
                System.IO.StreamReader sOut = proc.StandardOutput;
                // Attach the in for writing
                System.IO.StreamWriter sIn = proc.StandardInput;
                // Write each line of the batch file to standard input
                /*
                while(strm.Peek() != -1)
                {
                  sIn.WriteLine(strm.ReadLine());
                }
                strm.Close();
                */
                // Exit CMD.EXE
                    //string stEchoFmt = “# {0} run successfully. Exiting”;
                    //sIn.WriteLine(String.Format(stEchoFmt, strFilePath));
                    //sIn.WriteLine(“EXIT”);
                // Close the process
                proc.Close();

                // Read the sOut to a string.
                string results = sOut.ReadToEnd().Trim();



                // Close the io Streams;
                sIn.Close(); 
                sOut.Close();
                return "ejecuto";
            }
            catch (Exception ex) { 
                return ex.Message;
            }
        }
        public string testeo2(string Email, string Nombre, string Apellido, string Password)
        {
            try
            {
                string filename = Path.Combine(@"C:\\Users\\amedia\\Desktop\\EnviarMailsCambioPasswordSidepro\\", "testeoMailsBat.exe");
                var proc = System.Diagnostics.Process.Start(filename, Email + " " + Nombre + " " + Apellido + " " + Password);
    //            To kill/exit the program again, you can use following code:

                proc.CloseMainWindow(); 
                proc.Close();
                

                return "todo bien";
                        }
            catch (Exception ex) { 
                return ex.Message;
            }
        }

        public static string gmail_send(string Email, string Nombre, string Apellido, string Password)
        {
            string body = "";
            body = body + @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>";
            body = body + @"<html xmlns='http://www.w3.org/1999/xhtml'>";
            body = body + @"<head>";
            body = body + @"<title>Recupera tu contraseña</title>";
            body = body + @"<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
            body = body + @"<meta name='viewport' content='width=device-width, initial-scale=1.0' />";
            body = body + @"<style type='text/css'>*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%}a{outline:0;color:#fda50c;text-decoration:none}a:hover{text-decoration:underline!important}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important}.link a{color:#fff;text-decoration:underline}.link a:hover{text-decoration:none!important}.active:hover{opacity:.8}.active{-webkit-transition:all .3s ease;-moz-transition:all .3s ease;-ms-transition:all .3s ease;transition:all .3s ease}.footer a{color:#fff}table td{border-collapse:collapse!important;mso-line-height-rule:exactly}.ExternalClass,.ExternalClass a,.ExternalClass span,.ExternalClass b,.ExternalClass br,.ExternalClass p,.ExternalClass div{line-height:inherit}@media only screen and (max-width:500px){table[class='flexible']{width:100%!important}*[class='hide']{display:none!important;width:0!important;height:0!important;padding:0!important;font-size:0!important;line-height:0!important}td[class='img-flex'] img{width:100%!important;height:auto!important}td[class='aligncenter']{text-align:center!important}th[class='flex']{display:block!important;width:100%!important}tr[class='table-holder']{display:table!important;width:100%!important}th[class='thead']{display:table-header-group!important;width:100%!important}th[class='tfoot']{display:table-footer-group!important;width:100%!important}td[class='wrapper']{padding:5px!important}td[class='header']{padding:30px 10px!important}td[class='header'] *{text-align:center!important}td[class='title']{padding:0 0 30px!important;font-size:30px!important;line-height:36px!important}td[class='frame']{padding:20px 10px!important}td[class='holder']{padding:20px 20px 30px!important}td[class='holder'] *{text-align:center!important}td[class='block']{padding:0 10px 10px!important}td[class='box']{padding:0 20px 20px!important}td[class='nav']{line-height:30px!important}td[class='p-30']{padding:0 0 30px!important}td[class='p-20']{padding:0 0 20px!important}td[class='p-10']{padding:0 0 10px!important}td[class='block-10']{padding:0 10px 20px!important}td[class='indent-null']{padding:0!important}td[class='indent']{padding:0 10px!important}td[class='w-20']{width:20px!important}td[class='h-auto']{height:auto!important}td[class='footer']{padding:20px 10px!important}}</style>";
            body = body + @"</head>";
            body = body + @"<body style='margin:0;padding:0'><table style='min-width:320px' width='100%' cellspacing='0' cellpadding='0'><tr><td style='line-height:0'><div style='display:none;white-space:nowrap;font:15px/1px courier'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td></tr><tr><td class='hide'><table width='700' cellpadding='0' cellspacing='0' style='width:700px!important'><tr><td style='min-width:700px;font-size:0;line-height:0'>&nbsp;</td></tr></table></td></tr></table><table style='min-width:320px' width='100%' cellspacing='0' cellpadding='0'><tr><td><table data-module='pre-header' width='100%' cellpadding='0' cellspacing='0' style='display:none'><tr><td data-bgcolor='bg-module' bgcolor='#32862B' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td data-color='text-pre-header' data-size='size text-pre-header' data-min='10' data-max='24' data-link-color='link text-pre-header color' data-link-style='text-decoration:none; color:#fff;' align='center' style='padding:9px 10px 11px;font:14px/19px Arial,Helvetica,sans-serif;color:#83d27d'></td></tr></table></td></tr></table><table data-module='header' width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-module' bgcolor='#4EB046' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td class='header' style='padding:38px 0 37px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><th class='flex' align='center' style='vertical-align:top;padding:0'><table width='100%' cellpadding='0' cellspacing='0'><tr><td><a href='#' style='text-decoration:none;color:#fff;display:block;width:78px;margin:auto'><img src='https://sidepro.com.ar/som/images/som-logo-header2.png' height='121' width='78' border='0' style='vertical-align:top' alt='SOM' /></a></td></tr></table></th></tr></table></td></tr></table></td></tr></table><table data-module='module-1' width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-module' bgcolor='#4EB046' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td class='img-flex'><a href='#'><img src='https://sidepro.com.ar/som/images/imgMail.jpg' height='318' width='700' border='0' style='vertical-align:top' /></a></td></tr></table></td></tr></table><table data-module='module-2' width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-module' bgcolor='#4EB046' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td class='frame' bgcolor='#ffffff' style='padding:28px 40px 20px'><table width='100%' cellpadding='0' cellspacing='0'><tr><td data-color='title' data-size='size title' data-min='20' data-max='40' data-link-color='link title color' data-link-style='text-decoration:underline; color:#000;' align='center' style='padding:0 0 15px;font:bold 24px/26px Arial,Helvetica,sans-serif;color:#000'>¡Hola " + Nombre + " " + Apellido + "!</td></tr><tr><td data-color='text' data-size='size text' data-min='10' data-max='26' data-link-color='link text color' data-link-style='text-decoration:underline; color:#000;' style='font:14px/25px Arial,Helvetica,sans-serif;color:#000'>Solicitaste que se enviara este email para recuperar tu contraseña. <br />Para ingresar deberas utilizar la siguiente contraseña y cambiarla una vez que hayas ingresado a la plataforma.</td></tr></table></td></tr></table></td></tr></table><table data-module='module-3' width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-module' class='block-10' bgcolor='#4EB046' style='padding:0 10px 50px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-block' class='frame' bgcolor='#ffffff' style='border-radius:0 0 3px 3px;padding:20px 40px 40px'><table width='100%' cellpadding='0' cellspacing='0'><tr><td><table width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-content' class='holder' bgcolor='#32862B' style='padding:36px 39px 40px;border-radius:3px'><table width='100%' cellpadding='0' cellspacing='0'><tr class='link'><td data-color='title' data-size='size title' data-min='20' data-max='40' data-link-color='link title color' data-link-style='text-decoration:underline; color:#fff;' align='center' style='padding:0 0 15px;font:bold 24px/26px Arial,Helvetica,sans-serif;color:#fff'>Tu contraseña</td></tr><tr class='link'><td data-color='title' data-size='size title' data-min='20' data-max='40' data-link-color='link title color' data-link-style='text-decoration:underline; color:#fff;' align='center' style='padding:0 0 15px;font:18px/20px Arial,Helvetica,sans-serif;color:#fff'>" + Password + "</td></tr><tr class='link' align='center'><td data-color='text' data-size='size text' data-min='10' data-max='26' data-link-color='link text color' data-link-style='text-decoration:underline; color:#fff;' style='padding:0 0 23px;font:14px/25px Arial,Helvetica,sans-serif;color:#fff'>Recuerda cambiarla cuando ingreses a la plataforma</td></tr><tr><td><table align='center' cellpadding='0' cellspacing='0' style='margin:0 auto'><tr><td data-bgcolor='bg-button' data-size='size button' data-min='10' data-max='26' class='active' bgcolor='#ffffff' align='center' style='mso-padding-alt:13px 20px 11px;border-radius:3px;font:bold 15px/17px Arial,Helvetica,sans-serif;color:#fda50c;text-transform:uppercase'><a href='http://sidepro.com.ar/som' style='text-decoration:none!important;color:#32862B!important;display:block;padding:13px 20px 11px'>Quiero ingresar</a></td></tr></table></td></tr></table></td></tr></table></td></tr></table></td></tr></table></td></tr></table><table data-module='footer' width='100%' cellpadding='0' cellspacing='0' style='display:none'><tr><td data-bgcolor='bg-module' bgcolor='#32862B' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td class='footer' style='padding:30px 20px'><table width='100%' cellpadding='0' cellspacing='0'><tr><td class='p-20' style='padding:0 0 20px'><table align='center' cellpadding='0' cellspacing='0' style='margin:0 auto'><tr><td><a class='active' href='https://www.facebook.com/Sindicato-de-Obreros-de-Maestranza-143264025813304'><img src='sidepro.com.ar/som/Images/ico-facebook.png' height='17' width='9' border='0' style='vertical-align:top' alt='facebook' /></a></td><td class='w-20' width='29'></td><td><a class='active' href='https://twitter.com/somaestranza'><img src='sidepro.com.ar/som/images/ico-twitter.png' height='15' width='19' border='0' style='vertical-align:top' alt='twitter' /></a></td></tr></table></td></tr><tr><td data-color='text' data-size='size text' data-min='10' data-max='26' data-link-color='link text color' data-link-style='text-decoration:none; color:#fff;' align='center' style='font:14px/25px Arial,Helvetica,sans-serif;color:#fff'><a href='http://www.som.org.ar'>www.som.org.ar</a></td></tr></table></td></tr></table></td></tr></table></td></tr></table></body></html>";
                
            using (MailMessage mailMessage =
            new MailMessage(new MailAddress(@"no-reply@amedia.com.ar"),
            new MailAddress(@"no-reply@amedia.com.ar","Sidepro-SOM")))
            {
                mailMessage.Body = "body";
                mailMessage.Subject = "subject";
                try
                {
                    SmtpClient SmtpServer = new SmtpClient();
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.Credentials =
                        new System.Net.NetworkCredential(@"no-reply@amedia.com.ar", "sotelo21");
                    SmtpServer.Port = 587;
                    SmtpServer.Host = "smtp.gmail.com";
                    SmtpServer.EnableSsl = true;
                    var mail = new MailMessage();
                    String[] addr = Email.Split(','); // toemail is a string which contains many email address separated by comma
                    mail.From = new MailAddress(@"no-reply@amedia.com.ar","Sidepro-SOM");
                    Byte i;
                    for (i = 0; i < addr.Length; i++)
                        mail.To.Add(addr[i]);
                    mail.Subject = "Recuperar Password Sidepro-SOM";
                    mail.Body = body;// "cuerpo browar";
                    mail.IsBodyHtml = true;
                    mail.DeliveryNotificationOptions =
                        DeliveryNotificationOptions.OnFailure;
                    //   mail.ReplyTo = new MailAddress(toemail);
                    mail.ReplyToList.Add(@"no-reply@amedia.com.ar");
                    SmtpServer.Send(mail);
                    return "";
                }
                catch (Exception ex)
                {
                    string exp = ex.ToString();

                    //Console.WriteLine("Mail Not Sent ... and ther error is " + exp);
                    return exp;
                }
            }
        }
    }


}
