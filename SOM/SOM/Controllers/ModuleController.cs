﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SOM.Application;
using SOM.ViewModels;
namespace SOM.Controllers
{
    public class ModuleController : Controller
    {

        ModuleCursosViewModelController moduleVieweModelController = new ModuleCursosViewModelController();

        public ActionResult Module(int? id_module, int? id_curso)
        {
            ViewBag.Message = "Página de Modulos";
            int id_usuario = obtenerUsuario();
            return View(moduleVieweModelController.getCursosViewModel((int)id_module, id_curso, id_usuario));
        }
        public int obtenerUsuario()
        {
            int id_usuario = -1;
            if (HttpContext.Session["IDUsuario"] != null)
            {
                id_usuario = (int)HttpContext.Session["IDUsuario"];
            }
            else
            {
                id_usuario = new indexViewModel().id_usuario;
            }
            return id_usuario;
        }
    }
}
