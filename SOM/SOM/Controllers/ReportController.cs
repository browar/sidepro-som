﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SOM.Application;
using SOM.ViewModels;
using SOM.Entities;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using ClosedXML.Excel;
using CrystalDecisions.Shared;
using SOM.CrystalReports;
using System.Globalization;
namespace SOM.Controllers 
{
    public class ReportController : Controller
    {
        public ActionResult Index()
        {
            ReportViewModelController viewModelController = new ReportViewModelController();

            int id_usuario = obtenerUsuario();
            ReportViewModel miViewModel = viewModelController.getReportViewModel(id_usuario);
            return View(miViewModel);
        }
        public ActionResult listadoAgentes()
        {
            ReportViewModelController viewModelController = new ReportViewModelController();

            int id_usuario = obtenerUsuario();
            ReportViewModel miViewModel = viewModelController.getReportViewModel(id_usuario);
            return View(miViewModel);
        }
        public int obtenerUsuario()
        {
            int id_usuario = -1;
            if (HttpContext.Session["IDUsuario"] != null)
            {
                id_usuario = (int)HttpContext.Session["IDUsuario"];
            }
            else
            {
                id_usuario = new indexViewModel().id_usuario;
            }
            return id_usuario;
        }
        public ActionResult GetPagina(int id_pagina)
        {
            ReportManagementService reportManagementService = new ReportManagementService();
            int desde = (id_pagina - 1) * 6;
            int hasta = (id_pagina) * 6;
            var misDatos = reportManagementService.getProgresoAgente(desde, hasta);
            var json = new JavaScriptSerializer().Serialize(misDatos);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetUsuarios(string usuario) {

            ReportManagementService reportManagementService = new ReportManagementService();
            var misDatos = reportManagementService.getProgresoUsuarios(usuario);
            var json = new JavaScriptSerializer().Serialize(misDatos);
            return Json(json, JsonRequestBehavior.AllowGet);        
        }
        public ActionResult agente(int id_agencia)
        {
            ReportViewModelController viewModelController = new ReportViewModelController();

            int id_usuario = obtenerUsuario();
            ReportAgenciaViewModel miViewModel = viewModelController.getReportAgenciaViewModel(id_usuario, id_agencia);
            return View(miViewModel);
        }
        public ActionResult usuario(int id_usuario)
        {
            ReportViewModelController viewModelController = new ReportViewModelController();
            var miViewModel = viewModelController.getReportUsuarioViewModel(id_usuario);
            return View(miViewModel);
        }
        public ActionResult GetUsuariosAgencia(string usuario, int id_agencia)
        {

            ReportManagementService reportManagementService = new ReportManagementService();
            var misDatos = reportManagementService.getProgresoUsuarios(usuario, id_agencia);
            var json = new JavaScriptSerializer().Serialize(misDatos);
            return Json(json, JsonRequestBehavior.AllowGet);
        }


        public void ExportToExcel(int id_agencia)
        {
            
            ReportManagementService reportManagementService = new ReportManagementService();
            //List<CourseProgress>
            var certificaciones = reportManagementService.getProgresoMensualAgencia(id_agencia).OrderByDescending(x => x.Ano).ThenByDescending(y => y.Mes);


            var libro = new XLWorkbook();

            IXLWorksheet hoja;
            foreach (var cert in certificaciones)
            {

                int mes = cert.Mes;
                int ano = cert.Ano;
                var progresoUsuarios = reportManagementService.getCertificacionesDeUsuariosDeAgenciaDelMes(id_agencia, mes, ano);
                hoja = libro.Worksheets.Add(cert.Periodo);
                int cantidadUsuarios = 0;

                //Hoja
                //Titulo
                hoja.Cell("B2").Value = "Certificacion de " + cert.Periodo;
                //Valores agencia
                hoja.Cell("B3").Value = "Puntajes";
                hoja.Cell("C3").Value = cert.Total.ToString("0.00") + "%";//"75.34%";

                //Titulo 2
                hoja.Cells("B5").Value = "Documento";
                hoja.Cells("C5").Value = "Nombre";
                hoja.Cells("D5").Value = "Apellido";
                hoja.Cells("E5").Value = "Puntaje";

                foreach (var userProgress in progresoUsuarios)
                {
                    hoja.Cells("B" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Documento;
                    hoja.Cells("C" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Nombre ;
                    hoja.Cells("D" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Apellido;
                    hoja.Cells("E" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Certificacion.ToString() + (userProgress.Certificacion.ToString() != "No Realizado" ? "%" : "");
                    cantidadUsuarios++;
                }
                #region - Generar Rangos -

                var rangoAgencia = "B2:C3";
                var cellDocumento = "B5";
                var cellNombre = "C5";
                var cellApellido = "D5";
                var cellPuntaje = "E5";
                var rangoUsuarios = "B6:E" + (5 + cantidadUsuarios).ToString();
                var tablaUsuarios = "B5:E" + (5 + cantidadUsuarios).ToString();

                #endregion

                #region - Generar Formato -

                var rngTableAgencia = hoja.Range(rangoAgencia);
                rngTableAgencia.FirstRow().Merge();
                rngTableAgencia.FirstCell().Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    ;
                rngTableAgencia.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                rngTableAgencia.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                //var rngTableUsuarios = hoja.Range("B5:C5");
                hoja.Cells(cellDocumento).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Cells(cellNombre).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Cells(cellApellido).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Cells(cellPuntaje).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Columns().AdjustToContents();

                hoja.Column("B").Width = 20;
                hoja.Column("C").Width = 20;
                hoja.Column("D").Width = 20;
                hoja.Column("E").Width = 20;

                var rngTableUsuarios = hoja.Range(rangoUsuarios);
                rngTableUsuarios.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                rngTableUsuarios = hoja.Range(tablaUsuarios);
                rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                #endregion
            }
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"Certificaciones.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();

        }

        public ActionResult ReportePDF(int id_modulo, int id_curso,string nombreCurso) /*FileStreamResult */
        {
            string Nombre = "";
            string Apellido=""; 
            if (HttpContext.Session["IDUsuario"] != null)
            {

                Nombre = HttpContext.Session["NombreUsuario"].ToString();
                Apellido = HttpContext.Session["ApellidoUsuario"].ToString();
            }
            else
            {
                var viewModel = new indexViewModel();
                Nombre = viewModel.nombreUsuario;
                Apellido = viewModel.apellidoUsuario;

            }
            

            Stream st;
            //CursoFinalizado miReporte = new CursoFinalizado();
            Diploma miReporte = new Diploma();
            string path = Path.Combine(Server.MapPath(@"~/CrystalReports"), "Diploma.rpt");
            miReporte.Load(path);
            miReporte.SetParameterValue("@Nombre", ToTitleCase(Nombre) + " " + ToTitleCase(Apellido));
            miReporte.SetParameterValue("@NombreCurso", nombreCurso);


            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            Stream stream = miReporte.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", "CertificadoCursoFinalizado.pdf");



        }
        public string ToTitleCase(string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }
    }
}