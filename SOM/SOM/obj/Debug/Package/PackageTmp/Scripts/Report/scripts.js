var viewportWidth = null;
var viewportHeight = null;

$(window).on("resize", function(){
	viewportWidth = $(window).width();
	viewportHeight = $(window).height();
}).resize();

$(document).ready(function() {

	$(".js-open-menu").click(function(){
		if ($('body').hasClass('ie')) {
			if ($(".header-menu").hasClass("is-opened")) {
				$(".header-menu").removeClass("is-opened").hide();
			} else {
				$(".header-menu").addClass("is-opened").show();
			}
		} else {
			if (!$(".header-menu").hasClass("is-animating")) {
				if ($(".header-menu").hasClass("is-opened")) {
					$(".header-menu").addClass("is-animating").removeClass("is-opened");
					setTimeout(function(){
						$(".header-menu").hide().removeClass("is-animating");
					}, 510);
				} else {
					$(".header-menu").addClass("is-animating");
					$(".header-menu").show(0, function(){
						$(".header-menu").addClass("is-opened");
					});
					setTimeout(function(){
						$(".header-menu").removeClass("is-animating");
					}, 510);
				}
			}
		}
	});

	$('.js-course-hover').hover(function(){
		$(this).children().children('.js-course-btn-container').toggleClass('is-visible');
	});

	$('html').on('change', '.js-pic-update-upload-input', function(){
		$('.js-pic-update-upload, .js-pic-update-instructions').fadeOut(350, function(){
			$('.js-pic-update-loading').fadeIn(350);
		});
	});

	$('.js-module-grid').masonry({
		itemSelector: '.module__course'
	});

	$('.js-course-description-ellipsis').dotdotdot({
		watch: 'window'
	});

	$('.js-tooltip-right').tooltipster({
		animation: 'grow',
		delay: 0,
		trigger: 'hover',
		position: 'right'
	});
});