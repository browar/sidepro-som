var quizJSON = {
    "info": {
        "name":    "Samsung Galaxy S6 - Caracteristicas Generales",
        "main":    "",
        "results": "&nbsp;"
    },
    "questions": [
        {
            "q": "¿Cuanta memoria RAM tiene el equipo?",
            "a": [
                {"option": "512 MB",  "correct": false},
                {"option": "1 GB",    "correct": false},
                {"option": "3 GB",    "correct": true},
                {"option": "4 GB",    "correct": false}
            ],
            "correct": '<p>Su alta cantidad de RAM le otorga excelentes capacidades de multitasking.</p>',
            "incorrect": '<p>El equipo tiene 3 GB de RAM, lo cual le otorga excelentes capacidades de multitasking.</p>'
        },
        {
            "q": "¿Qué resolución tiene la pantalla del equipo?",
            "a": [
                {"option": "720p (1280x720)",   "correct": false},
                {"option": "1080p (1920x1080)", "correct": false},
                {"option": "QHD (2560x1440)",   "correct": true},
                {"option": "4K (3840x2160)",    "correct": false}
            ],
            "correct": "<p>Por el momento, muy pocos equipos llegan a tener resolución 4K.</p>",
            "incorrect": "<p>La pantalla tiene resolución QHD. Por el momento, muy pocos equipos llegan a tener resolución 4K.</p>"
        }
    ]
};