var quizJSON = {
    "info": {
        "name":    "Delegados Gremiales",
        "main":    "",
        "results": "&nbsp;"
    },
    "questions": [
        {
            "q": "¿Por qué es importante asistir a las reuniones?",
            "a": [
                {"option": "Para defender los intereses de los trabajadores",  "correct": false},
                {"option": "Para presentar propuestas",    "correct": false},
                {"option": "Ambas son correctas",    "correct": true}
            ],
            "correct": '<p>Ambas respuestas son correctas.</p>',
            "incorrect": '<p>Ambas respuestas son correctas.</p>'
        },
        {
            "q": "¿Cuál es la función principal del delegado?",
            "a": [
                {"option": "Resolver conflictos",   "correct": false},
                {"option": "Ser el enlace entre la UPCN, los trabajadores y las autoridades", "correct": true},
                {"option": "Ganar las elecciones",   "correct": false}
            ],
            "correct": "<p>Su función principal es la de ser el enlace entre la UPCN, los trabajadores y las autoridades.</p>",
            "incorrect": "<p>Su función principal es la de ser el enlace entre la UPCN, los trabajadores y las autoridades.</p>"
        }
    ]
};